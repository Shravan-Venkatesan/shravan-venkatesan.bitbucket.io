var socket = new WebSocket("wss://shravan-venkatesan.bitbucket.io/Processing/lib/server.js:8080");

function sendText() {
    
    // Construct a msg object containing the data the server needs to process the message from the chat client.
    var msg = {
        type: "message",
        text: document.getElementById("chat").value,
        id:   0,
        date: Date.now()
    };
    
    // Send the msg object as a JSON-formatted string.
    if (socket.readyState == OPEN) {
        socket.send(JSON.stringify(msg));
        document.getElementById("chat").value = "";
    }
    else {
        console.log("MESSAGE SEND FAILURE");
        console.log(socket.readyState);
    }
    
}

socket.onMessage = function (event) {
    
    console.log("MESSAGE RECEIVED");
    console.log(event.data);
    
}
