boolean WR1M, WR2M, BR1M, BR2M, WKM, BKM;
Subsquare WK, BK, selected_piece;
char player;
Gameboard board;

void setup() {
    
    if (window.innerWidth > window.innerHeight) {
       size(window.innerHeight, window.innerHeight);
    }
    else {
       size(window.innerWidth, window.innerWidth);
    }
    textSize(75);
    int[] dim = {8, 8};
    board = new Gameboard(dim);
    
    board.reset();
    // rando();
    
}
    
void draw() {
    
    background(0, 0, 156);
    
    board.make();
    
}
    
void rando() {
    
    board.find(4, 4).stat = "king";
    board.find(4, 4).c = 'W';
    board.find(1, 6).stat = "knight";
    board.find(1, 6).c = 'B';
    board.find(8, 5).stat = "rook";
    board.find(8, 5).c = 'B';
    board.find(6, 3).stat = "queen";
    board.find(6, 3).c = 'B';
    board.find(4, 5).stat = "bishop";
    board.find(4, 5).c = 'W';
    board.find(2, 5).stat = "pawn";
    board.find(2, 5).c = 'B';
    board.find(4, 3).stat = "pawn";
    board.find(4, 3).c = 'W';
    board.find(5, 1).stat = "bishop";
    board.find(5, 1).c = 'B';
    board.find(5, 6).stat = "knight";
    board.find(5, 6).c = 'B';
    
}
    
void pawn(float[] loc, char c) {
    
    if (c == 'W') {
        fill(256, 256, 256);
    }
    else if (c == 'B') {
        fill(0, 0, 0);
    }
    
    translate(((loc[0]-0.5)*200+40)*width/1680, height-(40+(loc[1]-0.5)*200)*height/1680);
    beginShape();
    vertex(-60*width/1680, +80*height/1680);
    vertex(-60*width/1680, +40*height/1680);
    vertex(-20*width/1680, 0);
    vertex(-40*width/1680, -30*height/1680);
    vertex(-20*width/1680, -60*height/1680);
    vertex(-20*width/1680, -80*height/1680);
    vertex(+20*width/1680, -80*height/1680);
    vertex(+20*width/1680, -60*height/1680);
    vertex(+40*width/1680, -30*height/1680);
    vertex(+20*width/1680, 0);
    vertex(+60*width/1680, +40*height/1680);
    vertex(+60*width/1680, +80*height/1680);
    endShape(CLOSE);
    translate(-((loc[0]-0.5)*200+40)*width/1680, -(height-(40+(loc[1]-0.5)*200)*height/1680));
    
}
    
void rook(float[] loc, char c) {
    
    if (c == 'W') {
        fill(256, 256, 256);
    }
    else if (c == 'B') {
        fill(0, 0, 0);
    }
    
    translate(((loc[0]-0.5)*200+40)*width/1680, height-(40+(loc[1]-0.5)*200)*height/1680);
    beginShape(QUAD_STRIP);
    vertex(-60*width/1680, +80*height/1680);
    vertex(+60*width/1680, +80*height/1680);
    vertex(-60*width/1680, +60*height/1680);
    vertex(+60*width/1680, +60*height/1680);
    vertex(-40*width/1680, +40*height/1680);
    vertex(+40*width/1680, +40*height/1680);
    vertex(-40*width/1680, -40*height/1680);
    vertex(+40*width/1680, -40*height/1680);
    vertex(-60*width/1680, -60*height/1680);
    vertex(+60*width/1680, -60*height/1680);
    vertex(-60*width/1680, -80*height/1680);
    vertex(+60*width/1680, -80*height/1680);
    endShape(CLOSE);
    translate(-((loc[0]-0.5)*200+40)*width/1680, -(height-(40+(loc[1]-0.5)*200)*height/1680));
    
}
    
void knight(float[] loc, char c) {
    
    if (c == 'W') {
        fill(256, 256, 256);
    }
    else if (c == 'B') {
        fill(0, 0, 0);
    }
    
    translate(((loc[0]-0.5)*200+40)*width/1680, height-(40+(loc[1]-0.5)*200)*height/1680);
    beginShape();
    vertex(-40*width/1680, +80*height/1680);
    vertex(+60*width/1680, +80*height/1680);
    vertex(+60*width/1680, 0);
    vertex(+40*width/1680, -60*height/1680);
    vertex(0, -80*width/1680);
    vertex(-40*width/1680, -80*height/1680);
    vertex(-60*width/1680, +30*height/1680);
    vertex(-40*width/1680, +30*height/1680);
    vertex(-20*width/1680, +10*height/1680);
    vertex(0, 0);
    vertex(-40*width/1680, +60*height/1680);
    endShape(CLOSE);
    translate(-((loc[0]-0.5)*200+40)*width/1680, -(height-(40+(loc[1]-0.5)*200)*height/1680));
    
}
    
void bishop(float[] loc, char c) {
    
    if (c == 'W') {
        fill(256, 256, 256);
    }
    else if (c == 'B') {
        fill(0, 0, 0);
    }
    
    translate(((loc[0]-0.5)*200+40)*width/1680, height-(40+(loc[1]-0.5)*200)*height/1680);
    beginShape(QUAD_STRIP);
    vertex(-40*width/1680, +80*height/1680);
    vertex(+40*width/1680, +80*height/1680);
    vertex(-40*width/1680, +60*height/1680);
    vertex(+40*width/1680, +60*height/1680);
    vertex(-20*width/1680, +60*height/1680);
    vertex(+20*width/1680, +60*height/1680);
    vertex(-20*width/1680, +40*height/1680);
    vertex(+20*width/1680, +40*height/1680);
    vertex(-30*width/1680, +40*height/1680);
    vertex(+30*width/1680, +40*height/1680);
    vertex(-30*width/1680, +20*height/1680);
    vertex(+30*width/1680, +20*height/1680);
    vertex(-30*width/1680, 0);
    vertex(+30*width/1680, 0);
    vertex(-10*width/1680, -60*height/1680);
    vertex(+10*width/1680, -60*height/1680);
    vertex(-10*width/1680, -80*height/1680);
    vertex(+10*width/1680, -80*height/1680);
    endShape(CLOSE);
    translate(-((loc[0]-0.5)*200+40)*width/1680, -(height-(40+(loc[1]-0.5)*200)*height/1680));
    
}
    
void queen(float[] loc, char c) {
    
    if (c == 'W') {
        fill(256, 256, 256);
    }
    else if (c == 'B') {
        fill(0, 0, 0);
    }
    
    translate(((loc[0]-0.5)*200+40)*width/1680, height-(40+(loc[1]-0.5)*200)*height/1680);
    beginShape();
    vertex(-60*width/1680, +80*height/1680);
    vertex(-40*width/1680, +40*height/1680);
    vertex(+40*width/1680, +40*height/1680);
    vertex(-40*width/1680, +40*height/1680);
    vertex(-60*width/1680, -60*height/1680);
    vertex(-30*width/1680, -20*height/1680);
    vertex(-30*width/1680, -60*height/1680);
    vertex(-10*width/1680, -20*height/1680);
    vertex(0, -70*height/1680);
    vertex(+10*width/1680, -20*height/1680);
    vertex(+30*width/1680, -60*height/1680);
    vertex(+30*width/1680, -20*height/1680);
    vertex(+60*width/1680, -60*height/1680);
    vertex(+40*width/1680, +40*height/1680);
    vertex(+60*width/1680, +80*height/1680);
    endShape(CLOSE);
    translate(-((loc[0]-0.5)*200+40)*width/1680, -(height-(40+(loc[1]-0.5)*200)*height/1680));
    
}
    
void king(float[] loc, char c) {
    
    if (c == 'W') {
        fill(256, 256, 256);
    }
    else if (c == 'B') {
        fill(0, 0, 0);
    }
    
    translate(((loc[0]-0.5)*200+40)*width/1680, height-(40+(loc[1]-0.5)*200)*height/1680);
    beginShape();
    vertex(-40*width/1680, +80*height/1680);
    vertex(-60*width/1680, -60*height/1680);
    vertex(-20*width/1680, 0);
    vertex(0, -80*height/1680);
    vertex(+20*width/1680, 0);
    vertex(+60*width/1680, -60*height/1680);
    vertex(+40*width/1680, +80*height/1680);
    endShape(CLOSE);
    translate(-((loc[0]-0.5)*200+40)*width/1680, -(height-(40+(loc[1]-0.5)*200)*height/1680));
    
}
    
class Subsquare {
  
    float[] loc;
    String stat;
    char c;
    boolean selected;
    boolean movable;

    Subsquare(float[] loc, String stat, char c, boolean selected, boolean movable) {
        this.loc = loc;
        this.stat = stat;
        this.c = c;
        this.movable = movable;
        this.selected = selected;
    }
    
    Subsquare(float[] loc, String stat, char c) {
       this(loc, stat, c, false, false); 
    }
        
    void make() {
        
        stroke(56);
        strokeWeight(5);
        if ((this.loc[0] + this.loc[1]) % 2 == 0) {
            fill(135,206,250);
        }
        else {
            fill(153,50,204);
        }
        rect(((loc[0]-1)*200+40)*width/1680, height-(40+(loc[1])*200)*height/1680, 200*width/1680, 200*height/1680);
        
        if (this.stat == "pawn") {
            pawn(this.loc, this.c);
            if (this.loc[1] == 1 || this.loc[1] == 8) {
                float[] loc1 = {this.loc[0] - 0.5, this.loc[1] + 0.5};
                float[] loc2 = {this.loc[0] + 0.5, this.loc[1] + 0.5};
                float[] loc3 = {this.loc[0] + 0.5, this.loc[1] - 0.5};
                float[] loc4 = {this.loc[0] - 0.5, this.loc[1] - 0.5};
                rook(loc1, this.c);
                bishop(loc2, this.c);
                knight(loc3, this.c);
                queen(loc4, this.c);
            }
        }
        else if (this.stat == "rook") {
            rook(this.loc, this.c);
        }
        else if (this.stat == "knight") {
            knight(this.loc, this.c);
        }
        else if (this.stat == "bishop") {
            bishop(this.loc, this.c);
        }
        else if (this.stat == "queen") {
            queen(this.loc, this.c);
        }
        else if (this.stat == "king") {
            king(this.loc, this.c);
        }

        if (mousePressed) {
             
            if (((this.loc[0]-1)*200+40)*width/1680 < mouseX && mouseX < ((this.loc[0])*200+40)*width/1680 && height-(40+(this.loc[1])*200)*height/1680 < mouseY && mouseY < height-(40+(this.loc[1]-1)*200)*height/1680) {
                if (this.movable) {
                    String temp_stat = this.stat;
                    char temp_c = this.c;
                    this.stat = selected_piece.stat;
                    this.c = selected_piece.c;
                    selected_piece.stat = "-";
                    selected_piece.c = '-';
                    if (this.stat == "king") {
                        if (player == 'W') {
                            WK = this;
                        }
                        else if (player == 'B') {
                            BK = this;
                        }
                    }
                    if (player == 'W' && WK.check()) {
                        selected_piece.stat = this.stat;
                        selected_piece.c = this.c;
                        this.stat = temp_stat;
                        this.c = temp_c;
                        WK = selected_piece;
                    }
                    else if (player == 'B' && BK.check()) {
                        selected_piece.stat = this.stat;
                        selected_piece.c = this.c;
                        this.stat = temp_stat;
                        this.c = temp_c;
                        BK = selected_piece;
                    }
                    else {
                        float[][] locs = {{3, 1}, {7, 1}, {3, 8}, {7, 8}};
                        if (WK.loc.equals(locs[0]) && !WKM && !WR1M) {
                            board.find(4, 1).stat = "rook";
                            board.find(4, 1).c = 'W';
                            board.find(1, 1).stat = "-";
                            board.find(1, 1).c = '-';
                        }
                        if (WK.loc.equals(locs[1]) && !WKM && !WR2M) {
                            board.find(6, 1).stat = "rook";
                            board.find(6, 1).c = 'W';
                            board.find(8, 1).stat = "-";
                            board.find(8, 1).c = '-';
                        }
                        if (BK.loc.equals(locs[2]) && !BKM && !BR1M) {
                            board.find(4, 8).stat = "rook";
                            board.find(4, 8).c = 'B';
                            board.find(1, 8).stat = "-";
                            board.find(1, 8).c = '-';
                        }
                        if (BK.loc.equals(locs[3]) && !BKM && !BR2M) {
                            board.find(6, 8).stat = "rook";
                            board.find(6, 8).c = 'B';
                            board.find(8, 8).stat = "-";
                            board.find(8, 8).c = '-';
                        }
                        float[] pos = {-1000, -1000};
                        selected_piece = new Subsquare(pos, "-", '-');
                        board.pieceCheck();
                        if (player == 'W') {
                            player = 'B';
                        }
                        else if (player == 'B') {
                            player = 'W';
                        }
                    }
                }
                else if (this.c == player) {
                    this.selected = true;
                    selected_piece = this;
                }
                board.moveClear();
            }
            else if (this.stat == "pawn" && (this.loc[1] == 1 || this.loc[1] == 8)) {
                if (((loc[0]-1.5)*200+40)*width/1680 < mouseX && mouseX < ((loc[0]-0.5)*200+40)*width/1680 && height-(40+(loc[1]+0.5)*200)*height/1680 < mouseY && mouseY < height-(40+(loc[1]-0.5)*200)*height/1680) {
                    this.stat = "rook";
                }
                else if (((loc[0]-0.5)*200+40)*width/1680 < mouseX && mouseX < ((loc[0]+0.5)*200+40)*width/1680 && height-(40+(loc[1]+0.5)*200)*height/1680 < mouseY && mouseY < height-(40+(loc[1]-0.5)*200)*height/1680) {
                    this.stat = "bishop";
                }
                else if (((loc[0]-0.5)*200+40)*width/1680 < mouseX && mouseX < ((loc[0]+0.5)*200+40)*width/1680 && height-(40+(loc[1]-0.5)*200)*height/1680 < mouseY && mouseY < height-(40+(loc[1]-1.5)*200)*height/1680) {
                    this.stat = "knight";
                }
                else if (((loc[0]-1.5)*200+40)*width/1680 < mouseX && mouseX < ((loc[0]-0.5)*200+40)*width/1680 && height-(40+(loc[1]-0.5)*200)*height/1680 < mouseY && mouseY < height-(40+(loc[1]-1.5)*200)*height/1680) {
                    this.stat = "queen";
                }
            }
            else {
                this.selected = false;
            }
            
        }
        
        if (this.selected) {
            if (this.stat == "pawn") {
                this.move_pawn();
            }
            else if (this.stat == "rook") {
                this.move_sides(false);
            }
            else if (this.stat == "knight") {
                this.move_knight();
            }
            else if (this.stat == "bishop") {
                this.move_diags(false);
            }
            else if (this.stat == "queen") {
                this.move_sides(false);
                this.move_diags(false);
            }
            else if (this.stat == "king") {
                this.move_sides(true);
                this.move_diags(true);
                if (this.c == 'W' && !WKM) {
                    if (!WR1M && board.find(2, 1).stat == "-" && board.find(3, 1).stat == "-" && board.find(4, 1).stat == "-") {
                        board.find(3, 1).movable = true;
                    }
                    if (!WR2M && board.find(6, 1).stat == "-" && board.find(7, 1).stat == "-") {
                        board.find(7, 1).movable = true;
                    }
                }
                else if (this.c == 'B' && !BKM) {
                    if (!BR1M && board.find(2, 8).stat == "-" && board.find(3, 8).stat == "-" && board.find(4, 8).stat == "-") {
                        board.find(3, 8).movable = true;
                    }
                    if (!BR2M && board.find(6, 8).stat == "-" && board.find(7, 8).stat == "-") {
                        board.find(7, 8).movable = true;
                    }
                }
            }
            
        }
            
        if (this.movable) {
            fill(0, 256, 0);
            rect(((loc[0]-1)*200+40)*width/1680, height-(40+(loc[1])*200)*height/1680, 200*width/1680, 200*height/1680);
        }
        
    }
            
    void ghost(float[] offset, boolean adj) {
        
        float[] presence = {this.loc[0] + offset[0], this.loc[1] + offset[1]};
        boolean collision = false;
        
        while (1 <= presence[0] && presence[0] <= 8 && 1 <= presence[1] && presence[1] <= 8 && !collision) {
            if (board.find(presence[0], presence[1]).c == this.c) {
                collision = true;
            }
            else {
                board.find(presence[0], presence[1]).movable = true;
                if (board.find(presence[0], presence[1]).c != '-' || adj) {
                    collision = true;
                }
                else {
                    presence[0] += offset[0];
                    presence[1] += offset[1];
                }
            }
        }
        
    }
                    
    boolean danger_ghost(float[] offset, String dir) {
        
        float[] presence = {this.loc[0] + offset[0], this.loc[1] + offset[1]};
        boolean collision = false;
        boolean adj = true;
        
        while (1 <= presence[0] && presence[0] <= 8 && 1 <= presence[1] && presence[1] <= 8 && !collision) {
            Subsquare s = board.find(presence[0], presence[1]);
            if (s.c == this.c) {
                collision = true;
            }
            else {
                if (dir == "sides") {
                    if (s.stat == "rook" || s.stat == "queen" || s.stat == "king" && adj) {
                        return true;
                    }
                    else if (s.stat != "-") {
                        collision = true;
                    }
                }
                else if (dir =="diags") {
                    if (s.stat == "bishop" || s.stat == "queen" || s.stat == "king" && adj) {
                        return true;
                    }
                    else if (s.stat != "-") {
                        collision = true;
                    }
                }
                presence[0] += offset[0];
                presence[1] += offset[1];
                adj = false;
            }
        }
        return false;
        
    }
            
    void move_pawn() {
        
        if (this.c == 'W' && this.loc[1] < 8) {
            
            Subsquare f1 = board.find(this.loc[0], this.loc[1] + 1);
            if (f1.stat == "-") {
                f1.movable = true;
                
                if (this.loc[1] < 7) {
                    Subsquare f2 = board.find(this.loc[0], this.loc[1] + 2);
                    if (f2.stat == "-" && this.loc[1] == 2) {
                        f2.movable = true;
                    }
                }
                
            }
                    
            if (this.loc[0] > 1) {
                Subsquare fl = board.find(this.loc[0] - 1, this.loc[1] + 1);
                if (fl.c == 'B') {
                    fl.movable = true;
                }
            }
                    
            if (this.loc[0] < 8) {
                Subsquare fr = board.find(this.loc[0] + 1, this.loc[1] + 1);
                if (fr.c == 'B') {
                    fr.movable = true;
                }
            }
            
        }
            
        else if (this.c == 'B' && this.loc[1] > 1) {
            
            Subsquare f1 = board.find(this.loc[0], this.loc[1] - 1);
            if (f1.stat == "-") {
                f1.movable = true;
                
                if (this.loc[1] > 2) {
                    Subsquare f2 = board.find(this.loc[0], this.loc[1] - 2);
                    if (f2.stat == "-" && this.loc[1] == 7) {
                        f2.movable = true;
                    }
                }
                
            }
                    
            if (this.loc[0] > 1) {
                Subsquare fl = board.find(this.loc[0] - 1, this.loc[1] - 1);
                if (fl.c == 'W') {
                    fl.movable = true;
                }
            }
                    
            if (this.loc[0] < 8) {
                Subsquare fr = board.find(this.loc[0] + 1, this.loc[1] - 1);
                if (fr.c == 'W') {
                    fr.movable = true;
                }
            }
            
        }
        
    }
        
    void move_knight() {
        
        float[][] locations = {{this.loc[0] - 1, this.loc[1] - 2}, {this.loc[0] + 1, this.loc[1] - 2}, {this.loc[0] + 2, this.loc[1] - 1}, {this.loc[0] + 2, this.loc[1] + 1}, {this.loc[0] + 1, this.loc[1] + 2}, {this.loc[0] - 1, this.loc[1] + 2}, {this.loc[0] - 2, this.loc[1] + 1}, {this.loc[0] - 2, this.loc[1] - 1}};
        
        for (float[] l: locations) {
            if (1 <= l[0] && l[0] <= 8 && 1 <= l[1] && l[1] <= 8) {
                if (board.find(l[0], l[1]).c != this.c) {
                    board.find(l[0], l[1]).movable = true;
                }
            }
        }
        
    }
        
    void move_sides(boolean adj) {
        
        float[][] offsets = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
        this.ghost(offsets[0], adj);
        this.ghost(offsets[1], adj);
        this.ghost(offsets[2], adj);
        this.ghost(offsets[3], adj);
        
    }
        
    void move_diags(boolean adj) {
        
        float[][] offsets = {{-1, 1}, {1, 1}, {1, -1}, {-1, -1}};
        this.ghost(offsets[0], adj);
        this.ghost(offsets[1], adj);
        this.ghost(offsets[2], adj);
        this.ghost(offsets[3], adj);
        
    }
        
    boolean danger_pawn() {
        
        if (this.c == 'W' && this.loc[1] < 8) {
            try { 
                Subsquare fl = board.find(this.loc[0] - 1, this.loc[1] + 1);
                if (fl.stat == "pawn" && fl.c == 'B') {
                    return true;
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {
                return false;
            }
            try { 
                Subsquare fr = board.find(this.loc[0] + 1, this.loc[1] + 1);
                if (fr.stat == "pawn" && fr.c == 'B') {
                    return true;
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {
                return false;
            }
            return false;
        }
            
        else if (this.c == 'B' && this.loc[1] > 1) {
            try { 
                Subsquare fl = board.find(this.loc[0] - 1, this.loc[1] - 1);
                if (fl.stat == "pawn" && fl.c == 'W') {
                    return true;
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {
                return false;
            }
            try { 
                Subsquare fr = board.find(this.loc[0] + 1, this.loc[1] - 1);
                if (fr.stat == "pawn" && fr.c == 'W') {
                    return true;
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {
                return false;
            }
            return false;
        }
        return false;
        
    }
    
    boolean danger_knight() {
        
        float[][] locations = {{this.loc[0] - 1, this.loc[1] - 2}, {this.loc[0] + 1, this.loc[1] - 2}, {this.loc[0] + 2, this.loc[1] - 1}, {this.loc[0] + 2, this.loc[1] + 1}, {this.loc[0] + 1, this.loc[1] + 2}, {this.loc[0] - 1, this.loc[1] + 2}, {this.loc[0] - 2, this.loc[1] + 1}, {this.loc[0] - 2, this.loc[1] - 1}};
        
        for (float[] l: locations) {
            if (1 <= l[0] && l[0] <= 8 && 1 <= l[1] && l[1] <= 8) {
                if (board.find(l[0], l[1]).stat == "knight" && board.find(l[0], l[1]).c != this.c) {
                    return true;
                }
            }
        }
        return false;
        
    }
    
    boolean danger_sides() {
        
        float[][] directions = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
        if (this.danger_ghost(directions[0], "sides") || this.danger_ghost(directions[1], "sides") || this.danger_ghost(directions[2], "sides") || this.danger_ghost(directions[3], "sides")) {
            return true;
        }
        else {
            return false;
        }
        
    }
    
    boolean danger_diags() {
        
        float[][] directions = {{-1, 1}, {1, 1}, {1, -1}, {-1, -1}};
        if (this.danger_ghost(directions[0], "diags") || this.danger_ghost(directions[1], "diags") || this.danger_ghost(directions[2], "diags") || this.danger_ghost(directions[3], "diags")) {
            return true;
        }
        else {
            return false;
        }
        
    }
        
    boolean check() {
        
        if (this.danger_pawn() || this.danger_knight() || this.danger_sides() || this.danger_diags()) {
            return true;
        }
        else {
            return false;
        }
        
    }
    
}
            
class Gameboard {
  
    int[] dim;
    Subsquare[][] grid;
    
    Gameboard(int[] dim) {
        this.dim = dim;
        this.grid = new Subsquare[dim[1]][dim[0]];
        for (int j = 1; j < this.dim[1]+1; j++) {
            for (int i = 1; i < this.dim[0]+1; i++) {
                float[] pos = {i, j};
                grid[j-1][i-1] = new Subsquare(pos, "-", '-');
            }
        }
    }
        
    void make() {
        
        for (Subsquare[] j: this.grid) {
            for (Subsquare i: j) {
                i.make();
            }
        }
                
        if (this.find(1, 1).stat != "rook") {
            WR1M = true;
        }
        if (this.find(8, 1).stat != "rook") {
            WR2M = true;
        }
        if (this.find(1, 8).stat != "rook") {
            BR1M = true;
        }
        if (this.find(8, 8).stat != "rook") {
            BR2M = true;
        }
        
    }
                
    Subsquare find(float loc1, float loc2) {
        
        return this.grid[(int)loc2-1][(int)loc1-1];
        
    }
    
    void moveClear() {
        
        for (Subsquare[] r: this.grid) {
            for (Subsquare s: r) {
                s.movable = false;
            }
        }
        
    }
                
    void pieceCheck() {
        
        if (board.find(1, 1).stat != "rook" || board.find(1, 1).c != 'W') {
            WR1M = true;
        }
        if (board.find(8, 1).stat != "rook" || board.find(8, 1).c != 'W') {
            WR2M = true;
        }
        if (board.find(1, 8).stat != "rook" || board.find(1, 8).c != 'B') {
            BR1M = true;
        }
        if (board.find(8, 8).stat != "rook" || board.find(8, 8).c != 'B') {
            BR2M = true;
        }
        if (board.find(5, 1).stat != "king" || board.find(5, 1).c != 'W') {
            WKM = true;
        }
        if (board.find(5, 8).stat != "king" || board.find(5, 8).c != 'B') {
            BKM = true;
        }
        
    }
                
    void reset() {
        
        player = 'W';
        float[] pos = {-1000, -1000};
        selected_piece = new Subsquare(pos, "-", '-');
        WK = board.find(5, 1);
        BK = board.find(5, 8);
        WKM = false;
        BKM = false;
        WR1M = false;
        WR2M = false;
        BR1M = false;
        BR2M = false;
    
        for (Subsquare s: this.grid[0]) {
            s.c = 'W';
        }
        for (Subsquare s: this.grid[1]) {
            s.c = 'W';
            s.stat = "pawn";
        }
        for (Subsquare s: this.grid[6]) {
            s.c = 'B';
            s.stat = "pawn";
        }
        for (Subsquare s: this.grid[7]) {
            s.c = 'B';
        }
            
        this.grid[0][0].stat = "rook";
        this.grid[0][7].stat = "rook";
        this.grid[7][0].stat = "rook";
        this.grid[7][7].stat = "rook";
        
        this.grid[0][1].stat = "knight";
        this.grid[0][6].stat = "knight";
        this.grid[7][1].stat = "knight";
        this.grid[7][6].stat = "knight";
        
        this.grid[0][2].stat = "bishop";
        this.grid[0][5].stat = "bishop";
        this.grid[7][2].stat = "bishop";
        this.grid[7][5].stat = "bishop";
        
        this.grid[0][3].stat = "queen";
        this.grid[0][4].stat = "king";
        this.grid[7][3].stat = "queen";
        this.grid[7][4].stat = "king";
        
    }
    
}
    