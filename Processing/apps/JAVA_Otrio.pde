color empty;
Board gameboard;
Player[] players;
int turn;
Stack selected_stack;
int count = 0;
  
int[] primes = {2, 3, 5, 
              7, 11, 13, 
              17, 19, 23, 
              29, 31, 37, 
              41, 43, 47, 
              53, 59, 61, 
              67, 71, 73, 
              79, 83, 89, 
              97, 101, 103};
int[] wins = {2*3*5, 7*11*13, 17*19*23, 
            29*31*37, 41*43*47, 43*59*61, 
            67*71*73, 79*83*89, 97*101*103,
            2*7*17, 3*11*19, 5*13*23,
            29*41*53, 31*43*59, 37*47*61,
            67*79*97, 71*83*101, 73*89*103,
            2*11*23, 5*11*17, 29*43*47, 
            37*43*53, 67*83*103, 73*83*97,
            2*29*67, 3*31*71, 5*37*73,
            7*41*79, 11*43*83, 13*47*89,
            17*53*97, 19*59*101, 23*61*103,
            2*41*97, 3*43*101, 5*47*103,
            17*41*67, 19*43*71, 23*47*73,
            2*31*73, 7*43*89, 17*59*103,
            5*31*67, 13*43*79, 23*59*97,
            2*43*103, 5*43*97, 17*43*73, 23*43*67};

void setup() {
    
    count = 0;
    if (window.innerWidth > window.innerHeight) {
       size(window.innerHeight, window.innerHeight);
    }
    else {
       size(window.innerWidth, window.innerWidth);
    }
    background(255,235,205);
    strokeWeight(10);
    empty = color(225,205,175); 
    gameboard = new Board();
    Player[] player_arr = {new Player("Purple", 1, color(138,43,226)), new Player("Green", 2, color(127,255,0)), new Player("Blue", 3, color(0,191,255)), new Player("Red", 4, color(220,20,60))};
    players = player_arr;
    turn = 1;
    selected_stack = null;
    
}
    
void draw() {
    
    background(255,235,205);
    
    gameboard.make();
    for (Player player: players) {
        player.make();
    }
    
    if (selected_stack != null) {
        stroke(255);
        strokeWeight(15);
        fill(255, 100);
        rect(width/2 - (selected_stack.loc[0]*325 + 150)*width/2000, height/2 + (selected_stack.loc[1]*325 - 150)*height/2000, 300*width/2000, 300*height/2000);
        if (keyPressed) {
            if (key == '1') {
                try_move(players[turn - 1], selected_stack, 1);
            }
            else if (key == '2') {
                try_move(players[turn - 1], selected_stack, 2);
            }
            else if (key == '3') {
                try_move(players[turn - 1], selected_stack, 3);
            }
            else if (key == '0') {
                selected_stack = null;
            }
        }
    }
    
    if (keyPressed && key == 'r') {
        gameboard.reset();
        for (Player player: players) {
            player.reset();
        }
    }
        
    for (Player player: players) {
        int product = 1;
        for (Stack stack: gameboard.stacks) {
            for (Piece piece: stack.pieces) {
                if (piece.col == player.col) {
                    product *= piece.id;
                }
            }
        }
        for (int win_product: wins) {
            if (product % win_product == 0) {
                textSize(48);
                fill(0);
                String win_string = player.name + " wins!";
                text(win_string, 100*width/2000, 150*height/2000); 
            }
        }
    }
    
}
        
void select_stack(Stack stack) {
    
    selected_stack = stack;
    
}
    
void try_move(Player player, Stack stack, int sz) {
    
    for (Stack pstack: player.stacks) {
        if ((player.col == pstack.pieces[3 - sz].col) && (stack.pieces[3 - sz].col == empty)) {
            place_piece(pstack.pieces[3 - sz], stack.pieces[3 - sz]);
            break;
        }
    }
    
}

void place_piece(Piece piece, Piece to) {
    
    to.col = piece.col;
    piece.col = empty;
    selected_stack = null;
    next_turn();
    
}
            
void next_turn() {
    
    if (turn == 4) {
        turn = 1;
    }
    else {
        turn++;
    }
    
}
    
class Player {
  
    String name;
    int pos;
    color col;
    Stack[] stacks = new Stack[3];
    
    Player(String name, int pos, color col) {
        this.name = name;
        this.pos = pos;
        this.col = col;
        float[][] coords = new float[1][1];
        if (this.pos == 1) {
            float[][] c = {{-2.5, -1}, {-2.5, 0}, {-2.5, 1}};
            coords = c;
        }
        else if (this.pos == 2) {
            float[][] c = {{-1, 2.5}, {0, 2.5}, {1, 2.5}}; 
            coords = c;
        }
        else if (this.pos == 3) {
            float[][] c = {{2.5, 1}, {2.5, 0}, {2.5, -1}}; 
            coords = c;
        }
        else if (this.pos == 4) {
            float[][] c = {{1, -2.5}, {0, -2.5}, {-1, -2.5}}; 
            coords = c;
        }
        this.stacks[0] = new Stack(coords[0], false);
        this.stacks[1] = new Stack(coords[1], false);
        this.stacks[2] = new Stack(coords[2], false);
        for (Stack stack: this.stacks) {
            stack.on_board = false;
            for (Piece piece: stack.pieces) {
                piece.col = this.col;
            }
        }
        
    }
            
    void make() {
        
      for (Stack stack: this.stacks) {
            stack.make();
        }
        
    }
            
    void reset() {
        setup();
    }
    
}
        
class Stack {
    
    float[] loc;
    Piece[] pieces;
    boolean on_board;
  
    Stack(float[] loc, boolean on_board) {
        this.loc = loc;
        this.on_board = on_board;
        Piece[] p = {new Piece(this.loc, empty, 3, on_board), new Piece(this.loc, empty, 2, on_board), new Piece(this.loc, empty, 1, on_board)};
        this.pieces = p; 
    }
    Stack(float[] loc) {
        this.loc = loc;
        this.on_board = true;
        Piece[] p = {new Piece(this.loc, empty, 3, on_board), new Piece(this.loc, empty, 2, on_board), new Piece(this.loc, empty, 1, on_board)};
        this.pieces = p; 
    }
        
    void make() {
        if ((this.on_board) && (mouseX > width/2 - (this.loc[0]*325 + 150)*width/2000) && (mouseX < width/2 - (this.loc[0]*325 - 150)*width/2000) && (mouseY > height/2 + (this.loc[1]*325 - 150)*height/2000) && (mouseY < height/2 + (this.loc[1]*325 + 150)*height/2000)) {
            stroke(255);
            strokeWeight(15);
            noFill();
            rect(width/2 - (this.loc[0]*325 + 150)*width/2000, height/2 + (this.loc[1]*325 - 150)*height/2000, 300*width/2000, 300*height/2000);
            if (mousePressed) {
                select_stack(this);
            }
        }
        for (Piece piece: this.pieces) {
            piece.make();
        }
        
    }
    
}
        
class Piece {
    
    float[] loc;
    color col;
    int sz;
    int id;
    boolean on_board;
    
    Piece(float[] loc, color col, int sz, boolean on_board) {
        this.loc = loc;
        this.col = col;
        this.sz = sz;
        this.on_board = on_board;
        if (on_board) {
            this.id = primes[count];
            count += 1;
        }
    }
        
    void make() {
        fill(this.col);
        stroke(0);
        strokeWeight(10);
        ellipse(width/2 - (this.loc[0]*325)*width/2000, height/2 + (this.loc[1]*325)*height/2000, this.sz*100*width/2000, this.sz*100*height/2000);
    }
    
}
        
class Board {
  
    Stack[] stacks = new Stack[9];
    boolean won;
            
    Board() {
        for (int i = 0; i < 3; i++) { 
            for (int j = 0; j < 3; j++) {
                float[] pos = {i-1, j-1};
                stacks[3*i+j] = new Stack(pos);
            }
        }
        won = false;
    }
        
    void make() {
        for (Stack stack: this.stacks) {
            stack.make();
        }
    }
            
    void reset() {
        setup();
    }
    
}