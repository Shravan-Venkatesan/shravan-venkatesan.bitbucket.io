Board gameBoard;
char player;
String mode;
float selection;

void setup() {
    
    if (window.innerWidth > window.innerHeight) {
       size(window.innerHeight, window.innerHeight);
    }
    else {
       size(window.innerWidth, window.innerWidth);
    }
    textSize(75);
    
    int[] pos = {750, 750};
    gameBoard = new Board(pos);
    player = 'B';
    mode = "place";
    selection = -1;
    
}
    
void draw() {
    
    gameScreen();
    gameBoard.make();
    
}
    
void gameScreen() {
    
    background(206);
    noFill();
    stroke(0, 0, 0);
    strokeWeight(10);
    rect(150*width/1500, 150*height/1500, 1200*width/1500, 1200*height/1500); 
    line(750*width/1500, 150*height/1500, 750*width/1500, 1350*width/1500);
    line(150*width/1500, 750*height/1500, 1350*height/1500, 750*height/1500);
    strokeWeight(5);
    
}
    
void change(String x, float s) {
    
    if (x == "player") {
        if (player == 'B') {
            player = 'W';
        }
        else if (player == 'W') {
            player = 'B';
        }
    }
    else if (x == "mode") {
        int time = millis();
        while (millis() < (time + s*1000)) {
            continue;
        }
        if (mode == "place") {
            mode = "select";
        }
        else if (mode == "select") {
            mode = "rotate";
        }
        else if (mode == "rotate") {
            mode = "inter";
        }
        else if (mode == "inter") {
            mode = "place";
        }
    }
    else if (x == "selection") {
        selection = s;
    }
    
}    
    
class Subsquare {
  
    int[] loc;
    char stat;
    int t;
    
    Subsquare(int loc1, int loc2, int t) {
        int[] loc = {loc1, loc2};
        this.loc = loc;
        this.stat = '-';
        this.t = t;
    }
        
    void make() {
        
        if (this.stat == '-' && (this.loc[0]-100)*width/1500 < mouseX && mouseX < (this.loc[0]+100)*width/1500 && (this.loc[1]-100)*height/1500 < mouseY && mouseY < (this.loc[1]+100)*height/1500 && mousePressed && mode == "place") {
            if (player == 'B') {
                this.stat = 'B';
            }
            else if (player == 'W') {
                this.stat = 'W';
            }
            change("mode", 0.2);
        }
        
        fill(206, 0, 0);
        rect((this.loc[0]-100)*width/1500, (this.loc[1]-100)*height/1500, 200*width/1500, 200*height/1500);
        if (this.stat == 'B') {
            fill(0, 0, 0);
        }
        else if (this.stat == 'W') {
            fill(256, 256, 256);
        }
        else {
            fill(150, 0, 0);
        }
        ellipse(this.loc[0]*width/1500, this.loc[1]*height/1500, 125*width/1500, 125*height/1500);
        noFill();
        
    }
    
}
    
class Subboard {
  
    int[] loc;
    int t;
    boolean selected;
    Subsquare[] Board = new Subsquare[9];
    
    Subboard(int loc1, int loc2, int t) {
        int[] loc = {loc1, loc2};
        this.loc = loc;
        this.t = t;
        this.selected = false;
        Subsquare[] board = {new Subsquare(this.loc[0], this.loc[1], 0), new Subsquare(this.loc[0]-200, this.loc[1]-200, 1), new Subsquare(this.loc[0], this.loc[1]-200, 2), 
                             new Subsquare(this.loc[0]+200, this.loc[1]-200, 3), new Subsquare(this.loc[0]+200, this.loc[1], 4), new Subsquare(this.loc[0]+200, this.loc[1]+200, 5), 
                             new Subsquare(this.loc[0], this.loc[1]+200, 6), new Subsquare(this.loc[0]-200, this.loc[1]+200, 7), new Subsquare(this.loc[0]-200, this.loc[1], 8)};
        this.Board = board;
    }
        
    void make() {
        
        if (mousePressed) {
            
            if (mode == "select" && (this.loc[0]-300)*width/1500 < mouseX && mouseX < (this.loc[0]+300)*width/1500 && (this.loc[1]-300)*height/1500 < mouseY && mouseY < (this.loc[1]+300)*height/1500) {
            
                change("selection", this.t);
                this.selected = true;
                change("mode", 0.2);
                
            }
                
            else if (mode == "rotate") {
                
                char[] stats = new char[9];
                
                for (int i = 0; i < 9; i++) {
                    stats[i] = this.Board[i].stat;
                }
                
                if ((this.loc[0]-300)*width/1500 < mouseX && mouseX < this.loc[0]*width/1500 && (this.loc[1]-300)*height/1500 < mouseY && mouseY < (this.loc[1]+300)*height/1500 && this.selected) {
                    
                    for (int i = 1; i < 7; i++) {
                        this.Board[i].stat = stats[i+2];
                    }
                    this.Board[7].stat = stats[1];
                    this.Board[8].stat = stats[2];
                    
                    change("mode", 0.2);
                    change("player", -1);
                    
                }
                    
                else if (this.loc[0]*width/1500 < mouseX && mouseX < (this.loc[0]+300)*width/1500 && (this.loc[1]-300)*height/1500 < mouseY && mouseY < (this.loc[1]+300)*height/1500 && this.selected) {
                    
                    for (int i = 3; i < 9; i++) {
                        this.Board[i].stat = stats[i-2];
                    }
                    this.Board[1].stat = stats[7];
                    this.Board[2].stat = stats[8];
                    
                    change("mode", 0.2);
                    change("player", -1);
                    
                }
                
            }
            
        }
        
        for (int i = 0; i < 9; i++) {
            this.Board[i].make();
        }
        strokeWeight(10);
        if (mode == "select" || (selection == this.t && mode == "rotate")) {
            stroke(0, 206, 0);
            strokeWeight(20);
        }
            
        if (mode == "rotate" && selection == this.t) {
            
            strokeWeight(20);
            stroke(0, 256, 0);
            line((this.loc[0]-150)*width/1500, this.loc[1]*height/1500, (this.loc[0]-50)*width/1500, (this.loc[1]+100)*height/1500);
            line((this.loc[0]-150)*width/1500, this.loc[1]*height/1500, (this.loc[0]-50)*width/1500, (this.loc[1]-100)*height/1500);
            line((this.loc[0]+150)*width/1500, this.loc[1]*height/1500, (this.loc[0]+50)*width/1500, (this.loc[1]+100)*height/1500);
            line((this.loc[0]+150)*width/1500, this.loc[1]*height/1500, (this.loc[0]+50)*width/1500, (this.loc[1]-100)*height/1500);
            
        }
            
        rect((this.loc[0]-300)*width/1500, (this.loc[1]-300)*height/1500, 600*width/1500, 600*height/1500);
        strokeWeight(5);
        stroke(0, 0, 0);
        
        if (mode == "inter") {
            change("mode", 0);
        }
        
    }
    
}
    
class Board {
  
    int[] loc;
    char stat;
    Subboard[] Board = new Subboard[4];
    
    Board(int[] loc) {
        this.loc = loc;
        this.stat = '-';
        Subboard[] board = {new Subboard(this.loc[0]-310, this.loc[1]-310, 0), new Subboard(this.loc[0]+310, this.loc[1]-310, 1), 
                            new Subboard(this.loc[0]-310, this.loc[1]+310, 2), new Subboard(this.loc[0]+310, this.loc[1]+310, 3)};
        this.Board = board;
    }
        
    void make() {
        
        for (int i = 0; i < 4; i++) {
            this.Board[i].make();
        }
            
        if (this.Board[0].Board[1].stat == 'B' && this.Board[0].Board[2].stat == 'B' && this.Board[0].Board[3].stat == 'B' && this.Board[1].Board[1].stat == 'B' && this.Board[1].Board[2].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[8].stat == 'B' && this.Board[0].Board[0].stat == 'B' && this.Board[0].Board[4].stat == 'B' && this.Board[1].Board[8].stat == 'B' && this.Board[1].Board[0].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[7].stat == 'B' && this.Board[0].Board[6].stat == 'B' && this.Board[0].Board[5].stat == 'B' && this.Board[1].Board[7].stat == 'B' && this.Board[1].Board[6].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[1].stat == 'B' && this.Board[2].Board[2].stat == 'B' && this.Board[2].Board[3].stat == 'B' && this.Board[3].Board[1].stat == 'B' && this.Board[3].Board[2].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[8].stat == 'B' && this.Board[2].Board[0].stat == 'B' && this.Board[2].Board[4].stat == 'B' && this.Board[3].Board[8].stat == 'B' && this.Board[3].Board[0].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[7].stat == 'B' && this.Board[2].Board[6].stat == 'B' && this.Board[2].Board[5].stat == 'B' && this.Board[3].Board[7].stat == 'B' && this.Board[3].Board[6].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[1].Board[3].stat == 'B' && this.Board[0].Board[2].stat == 'B' && this.Board[0].Board[3].stat == 'B' && this.Board[1].Board[1].stat == 'B' && this.Board[1].Board[2].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[1].Board[4].stat == 'B' && this.Board[0].Board[0].stat == 'B' && this.Board[0].Board[4].stat == 'B' && this.Board[1].Board[8].stat == 'B' && this.Board[1].Board[0].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[1].Board[5].stat == 'B' && this.Board[0].Board[6].stat == 'B' && this.Board[0].Board[5].stat == 'B' && this.Board[1].Board[7].stat == 'B' && this.Board[1].Board[6].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[3].Board[3].stat == 'B' && this.Board[2].Board[2].stat == 'B' && this.Board[2].Board[3].stat == 'B' && this.Board[3].Board[1].stat == 'B' && this.Board[3].Board[2].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[3].Board[4].stat == 'B' && this.Board[2].Board[0].stat == 'B' && this.Board[2].Board[4].stat == 'B' && this.Board[3].Board[8].stat == 'B' && this.Board[3].Board[0].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[3].Board[5].stat == 'B' && this.Board[2].Board[6].stat == 'B' && this.Board[2].Board[5].stat == 'B' && this.Board[3].Board[7].stat == 'B' && this.Board[3].Board[6].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[1].stat == 'B' && this.Board[0].Board[8].stat == 'B' && this.Board[0].Board[7].stat == 'B' && this.Board[2].Board[1].stat == 'B' && this.Board[2].Board[8].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[2].stat == 'B' && this.Board[0].Board[0].stat == 'B' && this.Board[0].Board[6].stat == 'B' && this.Board[2].Board[2].stat == 'B' && this.Board[2].Board[0].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[3].stat == 'B' && this.Board[0].Board[4].stat == 'B' && this.Board[0].Board[5].stat == 'B' && this.Board[2].Board[3].stat == 'B' && this.Board[2].Board[4].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[1].Board[1].stat == 'B' && this.Board[1].Board[8].stat == 'B' && this.Board[1].Board[7].stat == 'B' && this.Board[3].Board[1].stat == 'B' && this.Board[3].Board[8].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[1].Board[2].stat == 'B' && this.Board[1].Board[0].stat == 'B' && this.Board[3].Board[6].stat == 'B' && this.Board[3].Board[2].stat == 'B' && this.Board[3].Board[0].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[1].Board[3].stat == 'B' && this.Board[1].Board[4].stat == 'B' && this.Board[1].Board[5].stat == 'B' && this.Board[3].Board[3].stat == 'B' && this.Board[3].Board[4].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[7].stat == 'B' && this.Board[0].Board[8].stat == 'B' && this.Board[0].Board[7].stat == 'B' && this.Board[2].Board[1].stat == 'B' && this.Board[2].Board[8].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[6].stat == 'B' && this.Board[0].Board[0].stat == 'B' && this.Board[0].Board[6].stat == 'B' && this.Board[2].Board[2].stat == 'B' && this.Board[2].Board[0].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[5].stat == 'B' && this.Board[0].Board[4].stat == 'B' && this.Board[0].Board[5].stat == 'B' && this.Board[2].Board[3].stat == 'B' && this.Board[2].Board[4].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[3].Board[7].stat == 'B' && this.Board[1].Board[8].stat == 'B' && this.Board[1].Board[7].stat == 'B' && this.Board[3].Board[1].stat == 'B' && this.Board[3].Board[8].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[3].Board[6].stat == 'B' && this.Board[1].Board[0].stat == 'B' && this.Board[3].Board[6].stat == 'B' && this.Board[3].Board[2].stat == 'B' && this.Board[3].Board[0].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[3].Board[5].stat == 'B' && this.Board[1].Board[4].stat == 'B' && this.Board[1].Board[5].stat == 'B' && this.Board[3].Board[3].stat == 'B' && this.Board[3].Board[4].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[1].stat == 'B' && this.Board[0].Board[0].stat == 'B' && this.Board[0].Board[5].stat == 'B' && this.Board[3].Board[1].stat == 'B' && this.Board[3].Board[0].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[3].Board[5].stat == 'B' && this.Board[0].Board[0].stat == 'B' && this.Board[0].Board[5].stat == 'B' && this.Board[3].Board[1].stat == 'B' && this.Board[3].Board[0].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[7].stat == 'B' && this.Board[2].Board[0].stat == 'B' && this.Board[2].Board[3].stat == 'B' && this.Board[1].Board[7].stat == 'B' && this.Board[1].Board[0].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[1].Board[3].stat == 'B' && this.Board[2].Board[0].stat == 'B' && this.Board[2].Board[3].stat == 'B' && this.Board[1].Board[7].stat == 'B' && this.Board[1].Board[0].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[2].stat == 'B' && this.Board[0].Board[4].stat == 'B' && this.Board[1].Board[7].stat == 'B' && this.Board[3].Board[2].stat == 'B' && this.Board[3].Board[4].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[8].stat == 'B' && this.Board[0].Board[6].stat == 'B' && this.Board[2].Board[3].stat == 'B' && this.Board[3].Board[8].stat == 'B' && this.Board[3].Board[6].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[8].stat == 'B' && this.Board[2].Board[2].stat == 'B' && this.Board[0].Board[5].stat == 'B' && this.Board[1].Board[8].stat == 'B' && this.Board[1].Board[2].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[6].stat == 'B' && this.Board[2].Board[4].stat == 'B' && this.Board[3].Board[1].stat == 'B' && this.Board[1].Board[6].stat == 'B' && this.Board[1].Board[4].stat == 'B') {
            fill(0, 0, 0);
            text("Player 1 Wins!", width/2-250, height*29/30); }
        
        
        
        else if (this.Board[0].Board[1].stat == 'W' && this.Board[0].Board[2].stat == 'W' && this.Board[0].Board[3].stat == 'W' && this.Board[1].Board[1].stat == 'W' && this.Board[1].Board[2].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[8].stat == 'W' && this.Board[0].Board[0].stat == 'W' && this.Board[0].Board[4].stat == 'W' && this.Board[1].Board[8].stat == 'W' && this.Board[1].Board[0].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[7].stat == 'W' && this.Board[0].Board[6].stat == 'W' && this.Board[0].Board[5].stat == 'W' && this.Board[1].Board[7].stat == 'W' && this.Board[1].Board[6].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[1].stat == 'W' && this.Board[2].Board[2].stat == 'W' && this.Board[2].Board[3].stat == 'W' && this.Board[3].Board[1].stat == 'W' && this.Board[3].Board[2].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[8].stat == 'W' && this.Board[2].Board[0].stat == 'W' && this.Board[2].Board[4].stat == 'W' && this.Board[3].Board[8].stat == 'W' && this.Board[3].Board[0].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[7].stat == 'W' && this.Board[2].Board[6].stat == 'W' && this.Board[2].Board[5].stat == 'W' && this.Board[3].Board[7].stat == 'W' && this.Board[3].Board[6].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[1].Board[3].stat == 'W' && this.Board[0].Board[2].stat == 'W' && this.Board[0].Board[3].stat == 'W' && this.Board[1].Board[1].stat == 'W' && this.Board[1].Board[2].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[1].Board[4].stat == 'W' && this.Board[0].Board[0].stat == 'W' && this.Board[0].Board[4].stat == 'W' && this.Board[1].Board[8].stat == 'W' && this.Board[1].Board[0].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[1].Board[5].stat == 'W' && this.Board[0].Board[6].stat == 'W' && this.Board[0].Board[5].stat == 'W' && this.Board[1].Board[7].stat == 'W' && this.Board[1].Board[6].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[3].Board[3].stat == 'W' && this.Board[2].Board[2].stat == 'W' && this.Board[2].Board[3].stat == 'W' && this.Board[3].Board[1].stat == 'W' && this.Board[3].Board[2].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[3].Board[4].stat == 'W' && this.Board[2].Board[0].stat == 'W' && this.Board[2].Board[4].stat == 'W' && this.Board[3].Board[8].stat == 'W' && this.Board[3].Board[0].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[3].Board[5].stat == 'W' && this.Board[2].Board[6].stat == 'W' && this.Board[2].Board[5].stat == 'W' && this.Board[3].Board[7].stat == 'W' && this.Board[3].Board[6].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[1].stat == 'W' && this.Board[0].Board[8].stat == 'W' && this.Board[0].Board[7].stat == 'W' && this.Board[2].Board[1].stat == 'W' && this.Board[2].Board[8].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[2].stat == 'W' && this.Board[0].Board[0].stat == 'W' && this.Board[0].Board[6].stat == 'W' && this.Board[2].Board[2].stat == 'W' && this.Board[2].Board[0].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[3].stat == 'W' && this.Board[0].Board[4].stat == 'W' && this.Board[0].Board[5].stat == 'W' && this.Board[2].Board[3].stat == 'W' && this.Board[2].Board[4].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[1].Board[1].stat == 'W' && this.Board[1].Board[8].stat == 'W' && this.Board[1].Board[7].stat == 'W' && this.Board[3].Board[1].stat == 'W' && this.Board[3].Board[8].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[1].Board[2].stat == 'W' && this.Board[1].Board[0].stat == 'W' && this.Board[3].Board[6].stat == 'W' && this.Board[3].Board[2].stat == 'W' && this.Board[3].Board[0].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[1].Board[3].stat == 'W' && this.Board[1].Board[4].stat == 'W' && this.Board[1].Board[5].stat == 'W' && this.Board[3].Board[3].stat == 'W' && this.Board[3].Board[4].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[7].stat == 'W' && this.Board[0].Board[8].stat == 'W' && this.Board[0].Board[7].stat == 'W' && this.Board[2].Board[1].stat == 'W' && this.Board[2].Board[8].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[6].stat == 'W' && this.Board[0].Board[0].stat == 'W' && this.Board[0].Board[6].stat == 'W' && this.Board[2].Board[2].stat == 'W' && this.Board[2].Board[0].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[5].stat == 'W' && this.Board[0].Board[4].stat == 'W' && this.Board[0].Board[5].stat == 'W' && this.Board[2].Board[3].stat == 'W' && this.Board[2].Board[4].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[3].Board[7].stat == 'W' && this.Board[1].Board[8].stat == 'W' && this.Board[1].Board[7].stat == 'W' && this.Board[3].Board[1].stat == 'W' && this.Board[3].Board[8].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[3].Board[6].stat == 'W' && this.Board[1].Board[0].stat == 'W' && this.Board[3].Board[6].stat == 'W' && this.Board[3].Board[2].stat == 'W' && this.Board[3].Board[0].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[3].Board[5].stat == 'W' && this.Board[1].Board[4].stat == 'W' && this.Board[1].Board[5].stat == 'W' && this.Board[3].Board[3].stat == 'W' && this.Board[3].Board[4].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[1].stat == 'W' && this.Board[0].Board[0].stat == 'W' && this.Board[0].Board[5].stat == 'W' && this.Board[3].Board[1].stat == 'W' && this.Board[3].Board[0].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[3].Board[5].stat == 'W' && this.Board[0].Board[0].stat == 'W' && this.Board[0].Board[5].stat == 'W' && this.Board[3].Board[1].stat == 'W' && this.Board[3].Board[0].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[7].stat == 'W' && this.Board[2].Board[0].stat == 'W' && this.Board[2].Board[3].stat == 'W' && this.Board[1].Board[7].stat == 'W' && this.Board[1].Board[0].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[1].Board[3].stat == 'W' && this.Board[2].Board[0].stat == 'W' && this.Board[2].Board[3].stat == 'W' && this.Board[1].Board[7].stat == 'W' && this.Board[1].Board[0].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[2].stat == 'W' && this.Board[0].Board[4].stat == 'W' && this.Board[1].Board[7].stat == 'W' && this.Board[3].Board[2].stat == 'W' && this.Board[3].Board[4].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[0].Board[8].stat == 'W' && this.Board[0].Board[6].stat == 'W' && this.Board[2].Board[3].stat == 'W' && this.Board[3].Board[8].stat == 'W' && this.Board[3].Board[6].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[8].stat == 'W' && this.Board[2].Board[2].stat == 'W' && this.Board[0].Board[5].stat == 'W' && this.Board[1].Board[8].stat == 'W' && this.Board[1].Board[2].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
        else if (this.Board[2].Board[6].stat == 'W' && this.Board[2].Board[4].stat == 'W' && this.Board[3].Board[1].stat == 'W' && this.Board[1].Board[6].stat == 'W' && this.Board[1].Board[4].stat == 'W') {
            fill(0, 0, 0);
            text("Player 2 Wins!", width/2-250, height*29/30); }
            
        noFill();
        
    }
    
}
        