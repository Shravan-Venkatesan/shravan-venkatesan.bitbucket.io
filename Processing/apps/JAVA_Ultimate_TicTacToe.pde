Board gameBoard;
char player;
int tag;

void setup() {
    
    if (window.innerWidth > window.innerHeight) {
       size(window.innerHeight, window.innerHeight);
    }
    else {
       size(window.innerWidth, window.innerWidth);
    }
    textSize(75);
    int[] board_loc = {750, 750};
    gameBoard = new Board(board_loc);
    player = 'X';
    tag = -1;
    
}
    
void draw() {
    
    gameScreen();
    gameBoard.make();
    if (player == 'O') {
        ellipse(750*width/1500, 150*height/1500, 200*width/1500, 200*height/1500);
    }
    else if (player == 'X') {
        line(650*width/1500, 50*height/1500, 850*width/1500, 250*height/1500);
        line(650*width/1500, 250*height/1500, 850*width/1500, 50*height/1500);
    }
    
}
        
void gameScreen() {
    
    background(206);
    noFill();
    stroke(0, 0, 0);
    strokeWeight(10);
    rect(300*width/1500, 300*height/1500, 900*width/1500, 900*height/1500);
    line(600*width/1500, 300*height/1500, 600*width/1500, 1200*height/1500);
    line(900*width/1500, 300*height/1500, 900*width/1500, 1200*height/1500);
    line(300*width/1500, 600*height/1500, 1200*width/1500, 600*height/1500);
    line(300*width/1500, 900*height/1500, 1200*width/1500, 900*height/1500);
    strokeWeight(5);
    
}
    
void changePlayer(int sent_tag) {
    
    if (player == 'X') {
        player = 'O';
    }
    else if (player == 'O') {
        player = 'X';
    }
    
    if (gameBoard.Board[sent_tag].stat != '-') {
        tag = -1;
    }
    else {
        tag = sent_tag;
    }
    
}
    
class Subsquare {
  
    int[] loc;
    char stat;
    int t;
    boolean perm;
    
    Subsquare(int[] loc, int t, boolean perm) { 
        this.loc = loc;
        this.stat = '-';
        this.t = t;
        this.perm = perm;
    }
    
    Subsquare(int[] loc, int t) { 
        this(loc, t, false);
    }
        
    void make() {
        
        if (this.stat == '-' && (this.loc[0]-50)*width/1500 < mouseX && mouseX < (this.loc[0]+50)*width/1500 && (this.loc[1]-50)*height/1500 < mouseY && mouseY < (this.loc[1]+50)*height/1500 && this.perm && mousePressed) {
            if (player == 'X') {
                this.stat = 'X';
                changePlayer(this.t);
            }
            else if (player == 'O') {
                this.stat = 'O';
                changePlayer(this.t);
            }
        }
        
        rect((this.loc[0]-50)*width/1500, (this.loc[1]-50)*height/1500, 100*width/1500, 100*height/1500);
        
        if (this.stat == 'O') {
            ellipse(this.loc[0]*width/1500, this.loc[1]*height/1500, 100*width/1500, 100*height/1500);
        }
        else if (this.stat == 'X') {
            line((this.loc[0]-50)*width/1500, (this.loc[1]-50)*height/1500, (this.loc[0]+50)*width/1500, (this.loc[1]+50)*height/1500);
            line((this.loc[0]-50)*width/1500, (this.loc[1]+50)*height/1500, (this.loc[0]+50)*width/1500, (this.loc[1]-50)*height/1500);
        }
        
    }
    
}
        
class Subboard {
    
    int[] loc;
    char stat;
    int t;
    boolean perm;
    Subsquare[] Board = new Subsquare[9];
  
    Subboard(int[] loc, int t, boolean perm) {
        this.loc = loc;
        this.stat = '-';
        this.t = t;
        this.perm = perm;
        for (int j = -1; j <= 1; j++) {
            for (int i = -1; i <= 1; i++) {
                int[] pos = {(this.loc[0]+100*i), (this.loc[1]+100*j)};
                int index = (j+1) * 3 + (i+1);
                this.Board[index] = new Subsquare(pos, index);
            }
        }
    }
    
    Subboard(int[] loc, int t) {
       this(loc, t, true); 
    }
        
    void make() {
        
        if ((tag == this.t || tag == -1) && this.stat == '-') {
            fill(0, 206, 0, 50);
            this.perm = true;
        }
        else {
            noFill();
            this.perm = false;
        }
        
        for (int i = 0; i < 9; i++) {
            this.Board[i].perm = this.perm;
            this.Board[i].make();
        }
            
        noFill();
        
        if (this.Board[0].stat == 'X' && this.Board[3].stat == 'X' && this.Board[6].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[1].stat == 'X' && this.Board[4].stat == 'X' && this.Board[7].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[2].stat == 'X' && this.Board[5].stat == 'X' && this.Board[8].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[0].stat == 'X' && this.Board[1].stat == 'X' && this.Board[2].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[3].stat == 'X' && this.Board[4].stat == 'X' && this.Board[5].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[6].stat == 'X' && this.Board[7].stat == 'X' && this.Board[8].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[0].stat == 'X' && this.Board[4].stat == 'X' && this.Board[8].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[2].stat == 'X' && this.Board[4].stat == 'X' && this.Board[6].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[0].stat == 'O' && this.Board[3].stat == 'O' && this.Board[6].stat == 'O') {
            this.stat = 'O';
        }
        else if (this.Board[1].stat == 'O' && this.Board[4].stat == 'O' && this.Board[7].stat == 'O') {
            this.stat = 'O';
        }
        else if (this.Board[2].stat == 'O' && this.Board[5].stat == 'O' && this.Board[8].stat == 'O') {
            this.stat = 'O';
        }
        else if (this.Board[0].stat == 'O' && this.Board[1].stat == 'O' && this.Board[2].stat == 'O') {
            this.stat = 'O';
        }
        else if (this.Board[3].stat == 'O' && this.Board[4].stat == 'O' && this.Board[5].stat == 'O') {
            this.stat = 'O';
        }
        else if (this.Board[6].stat == 'O' && this.Board[7].stat == 'O' && this.Board[8].stat == 'O') {
            this.stat = 'O';
        }
        else if (this.Board[0].stat == 'O' && this.Board[4].stat == 'O' && this.Board[8].stat == 'O') {
            this.stat = 'O';
        }
        else if (this.Board[2].stat == 'O' && this.Board[4].stat == 'O' && this.Board[6].stat == 'O') {
            this.stat = 'O';
        }
        
        if (this.stat == 'O') {
            ellipse(this.loc[0]*width/1500, this.loc[1]*height/1500, 300*width/1500, 300*height/1500);
        }
        else if (this.stat == 'X') {
            line((this.loc[0]-150)*width/1500, (this.loc[1]-150)*height/1500, (this.loc[0]+150)*width/1500, (this.loc[1]+150)*height/1500);
            line((this.loc[0]-150)*width/1500, (this.loc[1]+150)*height/1500, (this.loc[0]+150)*width/1500, (this.loc[1]-150)*height/1500);
        }
            
        if (this.Board[0].stat != '-' && this.Board[1].stat != '-' && this.Board[2].stat != '-' && this.Board[3].stat != '-' && this.Board[4].stat != '-' && this.Board[5].stat != '-' && this.Board[6].stat != '-' && this.Board[7].stat != '-' && this.Board[8].stat != '-') { 
            this.stat = '0';
        }
            
        if (tag == this.t && this.stat != '-') {
            changePlayer(this.t);
        }
        
    }
    
}
        
class Board {
    
    int[] loc;
    char stat;
    Subboard[] Board = new Subboard[9];
    
    Board(int[] loc) {
        this.loc = loc;
        this.stat = '-';
        for (int j = -1; j <= 1; j++) {
            for (int i = -1; i <= 1; i++) {
                int[] pos = {(this.loc[0]+300*i), (this.loc[1]+300*j)};
                int index = (j+1) * 3 + (i+1);
                this.Board[index] = new Subboard(pos, index);
            }
        }
    }
        
    void make() {
        for (int i = 0; i < 9; i++) {
            this.Board[i].make();
        }
            
        if (this.Board[0].stat == 'X' && this.Board[3].stat == 'X' && this.Board[6].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[1].stat == 'X' && this.Board[4].stat == 'X' && this.Board[7].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[2].stat == 'X' && this.Board[5].stat == 'X' && this.Board[8].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[0].stat == 'X' && this.Board[1].stat == 'X' && this.Board[2].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[3].stat == 'X' && this.Board[4].stat == 'X' && this.Board[5].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[6].stat == 'X' && this.Board[7].stat == 'X' && this.Board[8].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[0].stat == 'X' && this.Board[4].stat == 'X' && this.Board[8].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[2].stat == 'X' && this.Board[4].stat == 'X' && this.Board[6].stat == 'X') {
            this.stat = 'X';
        }
        else if (this.Board[0].stat == 'O' && this.Board[3].stat == 'O' && this.Board[6].stat == 'O') {
            this.stat = 'O';
        }
        else if (this.Board[1].stat == 'O' && this.Board[4].stat == 'O' && this.Board[7].stat == 'O') {
            this.stat = 'O';
        }
        else if (this.Board[2].stat == 'O' && this.Board[5].stat == 'O' && this.Board[8].stat == 'O') {
            this.stat = 'O';
        }
        else if (this.Board[0].stat == 'O' && this.Board[1].stat == 'O' && this.Board[2].stat == 'O') {
            this.stat = 'O';
        }
        else if (this.Board[3].stat == 'O' && this.Board[4].stat == 'O' && this.Board[5].stat == 'O') {
            this.stat = 'O';
        }
        else if (this.Board[6].stat == 'O' && this.Board[7].stat == 'O' && this.Board[8].stat == 'O') {
            this.stat = 'O';
        }
        else if (this.Board[0].stat == 'O' && this.Board[4].stat == 'O' && this.Board[8].stat == 'O') {
            this.stat = 'O';
        }
        else if (this.Board[2].stat == 'O' && this.Board[4].stat == 'O' && this.Board[6].stat == 'O') {
            this.stat = 'O';
        }
            
        if (this.stat == 'X') {
            fill(0);
            noStroke();
            text("Player 1 Wins!", width/2-250, height*7/8);
            strokeWeight(5);
            noFill();
        }
        else if (this.stat == 'O') {
            fill(0);
            noStroke();
            text("Player 2 Wins!", width/2-250, height*7/8);
            strokeWeight(5);
            noFill();
        }
        
    }
    
}