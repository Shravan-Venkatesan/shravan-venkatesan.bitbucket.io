int ticks;
GameSpace space;
Snake Player1;
Pickup apple;
char dir;
int time;
int[] game_dim = {30, 15};
int sizeX = 100*game_dim[0] + 40;
int sizeY = 100*game_dim[1] + 40;
float aspect = window.innerWidth / window.innerHeight;

void setup() {
    
    ticks = 0;
    space = new GameSpace(game_dim);
    int[] snake_arr = {floor(random(1, game_dim[0]+1)), floor(random(1, game_dim[1]+1))};
    Player1 = new Snake(snake_arr);
    dir = '-';
    time = millis();
    
    if (sizeX / sizeY > aspect) {
       size(sizeX*window.innerWidth/sizeX, sizeY*window.innerWidth/sizeX);
    }
    else {
       size(sizeX*window.innerHeight/sizeY, sizeY*window.innerHeight/sizeY);
    }
    dropPickup();
    
}
    
void draw() {
    
    gameScreen();
    space.make();
    Player1.make();
    apple.make();
    
}
    
boolean tick() {
    
    if (millis() > time + 100) {
        time = millis();
        ticks = ticks + 1;
        return true;
    }
    else {
        return false;
    }
        
}
    
void gameScreen() {
    
    fill(0, 0, 0);
    stroke(0, 0, 256);
    strokeWeight(40);
    rect(20*width/3080, 20*height/1580, 3040*width/3080, 1540*height/1580);
    strokeWeight(5);
    stroke(0, 0, 0);
    
}
    
void dropPickup() {
    
    int[] arr = {floor(random(1, game_dim[0] + 1)), floor(random(1, game_dim[1] + 1))};
    apple = new Pickup(arr);
    if (apple.loc == Player1.loc || space.plane[apple.loc[1]-1][apple.loc[0]-1].filled) {
        dropPickup();
    }
        
}
    
void keyPressed() {
    
    if ((key == 'W' || key == 'w') && dir != 'D') {
        dir = 'U';
    }
    else if ((key == 'D' || key == 'd') && dir != 'L') {
        dir = 'R';
    }
    else if ((key == 'S' || key == 's') && dir != 'U') {
        dir = 'D';
    }
    else if ((key == 'A' || key == 'a') && dir != 'R') {
        dir = 'L';
    }
        
}
        
class Subspace {
  
    int[] loc;
    boolean filled;
    int passed;
    
    Subspace(int[] loc) {
        this.loc = loc;
        this.filled = false;
        this.passed = 0;
    }
    
    void make() {
        
        strokeWeight(1);
        stroke(50, 50, 50);
        fill(0, 0, 0);
        rect(((this.loc[0]-1)*100*30/game_dim[0]+40)*width/3080, height-(40+this.loc[1]*100*15/game_dim[1])*height/1580, 100*width/3080*30/game_dim[0], 100*height/1580*15/game_dim[1]);
        if (this.filled && this.passed + Player1.woosh > ticks) {
            fill(0, 256, 0);
            ellipse(((this.loc[0]-0.5)*100*30/game_dim[0]+40)*width/3080, height-(40+(this.loc[1]-0.5)*100*15/game_dim[1])*height/1580, 90*width/3080*30/game_dim[0], 90*height/1580*15/game_dim[1]);
            fill(0, 0, 0);
        }
        else {
            this.filled = false;
        }
        
    }
            
}
                                        
class GameSpace {
  
  int[] dim;
  Subspace[][] plane;
    
    GameSpace(int[] dim) {
        this.dim = dim;
        this.plane = new Subspace[dim[1]][dim[0]];
        
        for (int j = 1; j < this.dim[1] + 1; j++) {
           
            for (int i = 1; i < this.dim[0] + 1; i++) {
                int[] arr = {i, j};
                this.plane[j-1][i-1] = new Subspace(arr);
            }
            
        }
        
    }
            
    void make() {
        
        for (Subspace[] r: this.plane) {
            for (Subspace s: r) {
                s.make();
            }
        }
        
    }
    
}
                
class Snake {
    
    int[] loc;
    int woosh;
    boolean alive;
  
    Snake(int[] loc) {
        this.loc = loc;
        this.woosh = 1;
        this.alive = true;
    }
        
    void make() {
     
        fill(0, 256, 0);
        ellipse(((this.loc[0]-0.5)*100*30/game_dim[0]+40)*width/3080, height-(40+(this.loc[1]-0.5)*100*15/game_dim[1])*height/1580, 90*width/3080*30/game_dim[0], 90*height/1580*15/game_dim[1]);
        fill(0, 0, 0);
        
        this.move();
        
    }
        
    void move() {
        
        try {
            space.plane[this.loc[1]-1][this.loc[0]-1].passed = ticks;
            space.plane[this.loc[1]-1][this.loc[0]-1].filled = true;
        }
        catch (IndexOutOfBoundsException e) {
            setup();
        }
        
        if (tick() && this.alive) {
            if (dir == 'U') {
                this.loc[1] = this.loc[1] + 1;
                this.collisionCheck();
            }
            else if (dir == 'R') {
                this.loc[0] = this.loc[0] + 1;
                this.collisionCheck();
            }
            else if (dir == 'D') {
                this.loc[1] = this.loc[1] - 1;
                this.collisionCheck();
            }
            else if (dir == 'L') {
                this.loc[0] = this.loc[0] - 1;
                this.collisionCheck();
            }
            
        }
        
    }
            
    void collisionCheck() {       
        
        try {
            if (this.loc[0] == apple.loc[0] && this.loc[1] == apple.loc[1]) {
                this.woosh = this.woosh + 1;
                dropPickup();
            }
            else if (this.loc[0] < 1 || this.loc[0] > space.dim[0] || this.loc[1] < 1 || this.loc[1] > space.dim[1] || space.plane[this.loc[1]-1][this.loc[0]-1].filled) {
                this.alive = false;
                setup();
            }
        }
        catch (IndexOutOfBoundsException e) {
            setup();
        }
    }
    
}
        
class Pickup {
  
    int[] loc;
    
    Pickup(int[] loc) {
        this.loc = loc;
    }
        
    void make() {
        fill(256, 0, 0);
        ellipse(((this.loc[0]-0.5)*100*30/game_dim[0]+40)*width/3080, height-(40+(this.loc[1]-0.5)*100*15/game_dim[1])*height/1580, 90*width/3080*30/game_dim[0], 90*height/1580*15/game_dim[1]);
        fill(0, 0, 0);
    }    
        
}