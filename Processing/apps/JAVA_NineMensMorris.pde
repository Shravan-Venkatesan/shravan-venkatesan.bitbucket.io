char player;
GameBoard board;
String mode;
int t;
int turn;

void setup() {
    
    if (window.innerWidth > window.innerHeight) {
       size(window.innerHeight, window.innerHeight);
    }
    else {
       size(window.innerWidth, window.innerWidth);
    }
    background(160, 120, 200);
    
    int[] board_dim = {15, 15};
    board = new GameBoard(board_dim);
    player = 'w';
    mode = "set";
    t = millis();
    turn = 1;
    
}
    
void draw() {
    
    board.make();
    
}
    
class Space {
    
    int[] loc;
    char stat;
    boolean selected;
    boolean moveable;
    boolean milled;
  
    Space(int[] loc, char stat, boolean selected, boolean moveable, boolean milled) {
        this.loc = loc;
        this.stat = stat;
        this.selected = selected;
        this.moveable = moveable;
        this.milled = milled;
    }
    
    Space(int[] loc) {
       this(loc, '-', false, false, false); 
    }
        
    void make() {
        
        if (mouseX > width/2 + (100*this.loc[0]-50)*width/1800 && mouseX < width/2 + (100*this.loc[0]+50)*width/1800 && mouseY > height/2 - (100*this.loc[1]+50)*height/1800 && mouseY < height/2 - (100*this.loc[1]-50)*height/1800 && mousePressed && millis() > t + 500) {
            
            t = millis();
            
            if (mode == "set" && this.stat == '-') {
                this.stat = player;
                turn += 1;
                
                if (turn > 18) {
                    mode = "select";
                }
                
                for (Path path: board.paths) {
                    boolean found = false;
                    for (Space space: path.spaces) {
                        if (space.equals(this)) {
                            found = true; 
                        }
                    }
                    if (found && path.spaces[0].stat == player && path.spaces[1].stat == player && path.spaces[2].stat == player) {
                        path.milled = true;
                        for (Space space: path.spaces) {
                            space.milled = true;
                        }
                        mode = "mill";
                    }
                }
                
                if (player == 'w') {
                    player = 'b';
                }
                else if (player == 'b') {
                    player = 'w';
                }
                
            }
                    
            else if (mode == "mill" && this.stat == player && !this.milled) {
                for (Path path: board.paths) {
                    boolean found = false;
                    for (Space s: path.spaces) {
                        if (s.equals(this)) {
                           found = true; 
                        }
                    }
                    if (found && path.milled && path.spaces[0].stat == player) {
                        path.milled = false;
                        for (Space space: path.spaces) {
                            space.milled = false;
                        }
                    }
                }
                this.stat = '-';
                if (turn > 18) {
                    mode = "select";
                }
                else {
                    mode = "set";
                }
            }
                    
            else if ((mode == "select" || mode == "move") && this.stat == player) {
                for (Space space: board.spaces) {
                    space.selected = false;
                    space.moveable = false;
                }
                this.selected = true;
                for (Path path: board.paths) {
                    if (this == path.spaces[0] && path.spaces[1].stat == '-') {
                        path.spaces[1].moveable = true;
                    }
                    if (this == path.spaces[1] && path.spaces[0].stat == '-') {
                        path.spaces[0].moveable = true;
                    }
                    if (this == path.spaces[1] && path.spaces[2].stat == '-') {
                        path.spaces[2].moveable = true;
                    }
                    if (this == path.spaces[2] && path.spaces[1].stat == '-') {
                        path.spaces[1].moveable = true;
                    }
                }
                mode = "move";
            }
                
            else if (mode == "move" && this.moveable) {
                this.stat = player;
                for (Space space: board.spaces) {
                    if (space.selected) {
                        for (Path path: board.paths) {
                            boolean found = false;
                            for (Space space2: path.spaces) {
                                if (space2.equals(this)) {
                                   found = true; 
                                }
                            }
                            if (found && path.milled && path.spaces[0].stat == player) {
                                path.milled = false;
                                for (Space space2: path.spaces) {
                                    space2.milled = false;
                                }
                            }
                        }
                        space.stat = '-';
                        space.selected = false;
                    }
                    space.moveable = false;
                }
    
                if (turn > 18) {
                    mode = "select";
                }
                else {
                    mode = "set";
                }
                for (Path path: board.paths) {
                    boolean found = false;
                    for (Space space: path.spaces) {
                        if (space.equals(this)) {
                            found = true; 
                        }
                    }
                    if (found && path.spaces[0].stat == player && path.spaces[1].stat == player && path.spaces[2].stat == player) {
                        path.milled = true;
                        mode = "mill";
                    }
                }
                            
                if (player == 'w') {
                    player = 'b';
                }
                else if (player == 'b') {
                    player = 'w';
                }
            }
            
            for (Path path: board.paths) {
                if (path.milled) {
                    for (Space space: path.spaces) {
                        space.milled = true;
                    }
                }
            }
            boolean frees = false;
            for (Space space: board.spaces) {
                if (space.stat == 'w' && !space.milled) {
                    frees = true;
                }
            }
            if (!frees) {
                for (Space space: board.spaces) {
                    if (space.stat == 'w') {
                        space.milled = false;
                    }
                }
            }
            frees = false;
            for (Space space: board.spaces) {
                if (space.stat == 'b' && !space.milled) {
                    frees = true;
                }
            }
            if (!frees) {
                for (Space space: board.spaces) {
                    if (space.stat == 'b') {
                        space.milled = false;
                    }
                }
            }
        }
            
        if (this.selected) {
            strokeWeight(10);
            stroke(0, 255, 0);
        }
        else if (this.moveable) {
            strokeWeight(10);
            stroke(0, 0, 255);
        }
        else if (this.stat == player && mode == "mill") {
            strokeWeight(10);
            stroke(255, 0, 0);
        }
        else {
            strokeWeight(5);
            stroke(0, 0, 0);
        }
        if (this.stat == 'w') {
            fill(255, 255, 255);
        }
        else if (this.stat =='b') {
            fill(0);
        }
        else {
            fill(160,82,45);
        }
        ellipse(width/2 + (100*this.loc[0])*width/1800, height/2 - (100*this.loc[1])*height/1800, 100*width/1800, 100*height/1800);
        if (this.milled) {
            stroke(150);
            ellipse(width/2 + (100*this.loc[0])*width/1800, height/2 - (100*this.loc[1])*height/1800, 50*width/1800, 50*height/1800);
        }
        
    }
    
}

class GameBoard {
    
    int[] dim;
    char stat;
    Space[] spaces = new Space[24];
    Path[] paths = new Path[16];
  
    GameBoard(int[] dim) {
        this.dim = dim;
        this.stat = '-';
        int count = 0;
        for (int j = -6; j <= 6; j += 2) {
            if (j == 0) {
                for (int i = -6; i <= 6; i += 2) {
                    if (i == 0) {
                       continue; 
                    }
                    int[] pos = {i, j};
                    this.spaces[count] = new Space(pos);
                    count++;                    
                }
            }
            else {
                for (int i = -abs(j); i <= abs(j); i += abs(j)) {
                    int[] pos = {i, j};
                    this.spaces[count] = new Space(pos);
                    count++;
                }
            }
        }
        
        Path[] p = {new Path(this.spaces[0], this.spaces[1], this.spaces[2]), new Path(this.spaces[3], this.spaces[4], this.spaces[5]), new Path(this.spaces[6], this.spaces[7], this.spaces[8]), 
                      new Path(this.spaces[9], this.spaces[10], this.spaces[11]), new Path(this.spaces[12], this.spaces[13], this.spaces[14]), 
                      new Path(this.spaces[15], this.spaces[16], this.spaces[17]), new Path(this.spaces[18], this.spaces[19], this.spaces[20]), new Path(this.spaces[21], this.spaces[22], this.spaces[23]), 
                      new Path(this.spaces[0], this.spaces[9], this.spaces[21]), new Path(this.spaces[3], this.spaces[10], this.spaces[18]), new Path(this.spaces[6], this.spaces[11], this.spaces[15]), 
                      new Path(this.spaces[1], this.spaces[4], this.spaces[7]), new Path(this.spaces[16], this.spaces[19], this.spaces[22]), 
                      new Path(this.spaces[8], this.spaces[12], this.spaces[17]), new Path(this.spaces[5], this.spaces[13], this.spaces[20]), new Path(this.spaces[2], this.spaces[14], this.spaces[23])};
        this.paths = p;
                      
    }
    
    void make() {
        
        stroke(0);
        strokeWeight(5);
        fill(200,122,85);
        rect(width/2 - (100*this.dim[0]/2)*width/1800, height/2 - (100*this.dim[1]/2)*height/1800, 100*this.dim[0]*width/1800, 100*this.dim[1]*height/1800);
        
        for (Path path: this.paths) {
            path.make();
        }
        for (Space space: this.spaces) {
            space.make();
        }
        
    }
    
}
            
class Path {
  
    Space[] spaces = new Space[3];
    boolean milled;
    
    Path(Space space1, Space space2, Space space3) {
        this.spaces[0] = space1;
        this.spaces[1] = space2;
        this.spaces[2] = space3;
        this.milled = false;
    }
        
    void make() {
        
        stroke(0);
        strokeWeight(3);
        for (int i = 0; i < this.spaces.length - 1; i++) {
            line(width/2 + (100*this.spaces[i].loc[0])*width/1800, height/2 - (100*this.spaces[i].loc[1])*height/1800, width/2 + (100*this.spaces[i+1].loc[0])*width/1800, height/2 - (100*this.spaces[i+1].loc[1])*height/1800);
        }
        
    }
    
}
            