boolean force;
Subsquare selected_piece; 
char player;
Gameboard board;
int[] game_dim = {8, 8};

void setup() {

    if (window.innerWidth > window.innerHeight) {
       size(window.innerHeight, window.innerHeight);
    }
    else {
       size(window.innerWidth, window.innerWidth);	 
    }
    board = new Gameboard(game_dim);
    board.reset();
    player = 'r';
    force = false;
    selectReset();
    
}
        
void draw() {
    
    board.make();
    
}
    
void keyPressed() {
    
    if (key == ' ' && force) {
        force = false;
        if (player == 'r') {
            player = 'b';
        }
        else if (player == 'b') {     
            player = 'r';
        }
    }
    
}
    
void selectReset() {

    int[] pos = {-1000, -1000};
    selected_piece = new Subsquare(pos, '-');
    
}
    
class Subsquare {
    
    int[] loc;
    char stat;
    boolean selected;
    boolean crowned;
    boolean movable;

    Subsquare(int[] loc, char stat, boolean selected, boolean crowned, boolean movable) {
        this.loc = loc;
        this.stat = stat;
        this.selected = selected;
        this.crowned = crowned;
        this.movable = movable;
    }
    
    Subsquare(int[] loc, char stat) {
        this(loc, stat, false, false, false); 
    }
        
    void moveCheck() {
        
        int m = 0;
        
        if (this.stat == '-') {
            if (0 < this.loc[1] && this.loc[1] < game_dim[1]) {
                if (0 < this.loc[0] && this.loc[0] < game_dim[0]) {
                    if (board.grid[this.loc[1]][this.loc[0]].selected && ((board.grid[this.loc[1]][this.loc[0]].stat == 'b' && player == 'b') || board.grid[this.loc[1]][this.loc[0]].crowned)) {
                        this.movable = true;
                        m += 1;
                    }
                    else if (0 < this.loc[1] && this.loc[1] < game_dim[1] - 1 && 0 < this.loc[0] && this.loc[0] < game_dim[0] - 1) {
                         if (board.grid[this.loc[1]][this.loc[0]].stat != '-' && board.grid[this.loc[1]][this.loc[0]].stat != player && board.grid[this.loc[1]+1][this.loc[0]+1].selected && ((board.grid[this.loc[1]+1][this.loc[0]+1].stat == 'b' && player == 'b') || board.grid[this.loc[1]+1][this.loc[0]+1].crowned)) {
                            this.movable = true;
                            m += 1;
                         }
                    }
                }
                if (1 < this.loc[0] && this.loc[0] < game_dim[0] + 1) {
                    if (board.grid[this.loc[1]][this.loc[0]-2].selected && ((board.grid[this.loc[1]][this.loc[0]-2].stat == 'b' && player == 'b') || board.grid[this.loc[1]][this.loc[0]-2].crowned)) {
                        this.movable = true;
                        m += 1;
                    }
                    else if (0 < this.loc[1] && this.loc[1] < game_dim[1] - 1 && 2 < this.loc[0] && this.loc[0] < game_dim[0] + 1) {
                         if (board.grid[this.loc[1]][this.loc[0]-2].stat != '-' && board.grid[this.loc[1]][this.loc[0]-2].stat != player && board.grid[this.loc[1]+1][this.loc[0]-3].selected && ((board.grid[this.loc[1]+1][this.loc[0]-3].stat == 'b' && player == 'b') || board.grid[this.loc[1]+1][this.loc[0]-3].crowned)) {
                            this.movable = true;
                            m += 1;
                         }
                    }
                }
            }
            if (1 < this.loc[1] && this.loc[1] < game_dim[1] + 1) {
                if (0 < this.loc[0] && this.loc[0] < game_dim[0]) {
                    if (board.grid[this.loc[1]-2][this.loc[0]].selected && ((board.grid[this.loc[1]-2][this.loc[0]].stat == 'r' && player == 'r') || board.grid[this.loc[1]-2][this.loc[0]].crowned)) {
                        this.movable = true;
                        m += 1;
                    }
                    else if (2 < this.loc[1] && this.loc[1] < game_dim[1] + 1 && 0 < this.loc[0] && this.loc[0] < game_dim[0] - 1) {
                        if (board.grid[this.loc[1]-2][this.loc[0]].stat != '-' && board.grid[this.loc[1]-2][this.loc[0]].stat != player && board.grid[this.loc[1]-3][this.loc[0]+1].selected && ((board.grid[this.loc[1]-3][this.loc[0]+1].stat == 'r' && player == 'r') || board.grid[this.loc[1]-3][this.loc[0]+1].crowned)) {
                            this.movable = true;
                            m += 1;
                        }
                    }
                }
                if (1 < this.loc[0] && this.loc[0] < game_dim[0] + 1) {
                    if (board.grid[this.loc[1]-2][this.loc[0]-2].selected && ((board.grid[this.loc[1]-2][this.loc[0]-2].stat == 'r' && player == 'r') || board.grid[this.loc[1]-2][this.loc[0]-2].crowned)) {
                        this.movable = true;
                        m += 1;
                    }
                    else if (2 < this.loc[1] && this.loc[1] < game_dim[1] + 1 && 2 < this.loc[0] && this.loc[0] < game_dim[0] + 1) {
                        if (board.grid[this.loc[1]-2][this.loc[0]-2].stat != '-' && board.grid[this.loc[1]-2][this.loc[0]-2].stat != player && board.grid[this.loc[1]-3][this.loc[0]-3].selected && ((board.grid[this.loc[1]-3][this.loc[0]-3].stat == 'r' && player == 'r') || board.grid[this.loc[1]-3][this.loc[0]-3].crowned)) {
                            this.movable = true;
                            m += 1;
                        }
                    }
                }
            }
        
            if (m == 0) {
                this.movable = false;
            }
            
        }
        
    }
        
    void make() {
        
        stroke(0, 0, 0);
        strokeWeight(5);
        if ((this.loc[0] + this.loc[1]) % 2 == 1) {
            fill(206, 42, 42);
        }
        else {
            fill(36, 36, 36);
        }
        rect(((this.loc[0]-1)*200*8/game_dim[0]+40)*width/1680, (1640-this.loc[1]*200*8/game_dim[1])*height/1680, 200*width/1680*8/game_dim[0], 200*height/1680*8/game_dim[1]);
        
        if (this.loc[1] > 7 && this.stat == 'r' || this.loc[1] < 2 && this.stat == 'b') {
            this.crowned = true;
        }
        
        if (this.stat == 'r') {
            fill(256, 0, 0);
            ellipse(((this.loc[0]-0.5)*200*8/game_dim[0]+40)*width/1680, (1640-(-100+this.loc[1]*200)*8/game_dim[1])*height/1680, 175*width/1680*8/game_dim[0], 175*height/1680*8/game_dim[1]);
            ellipse(((this.loc[0]-0.5)*200*8/game_dim[0]+40)*width/1680, (1640-(-100+this.loc[1]*200)*8/game_dim[1])*height/1680, 125*width/1680*8/game_dim[0], 125*height/1680*8/game_dim[1]);
        }
        else if (this.stat == 'b') {
            fill(56, 56, 56);
            ellipse(((this.loc[0]-0.5)*200*8/game_dim[0]+40)*width/1680, (1640-(-100+this.loc[1]*200)*8/game_dim[1])*height/1680, 175*width/1680*8/game_dim[0], 175*height/1680*8/game_dim[1]);
            ellipse(((this.loc[0]-0.5)*200*8/game_dim[0]+40)*width/1680, (1640-(-100+this.loc[1]*200)*8/game_dim[1])*height/1680, 125*width/1680*8/game_dim[0], 125*height/1680*8/game_dim[1]);
        }
        if (this.crowned) {
            fill(256, 256, 0);
            ellipse(((this.loc[0]-0.5)*200*8/game_dim[0]+40)*width/1680, (1640-(-100+this.loc[1]*200)*8/game_dim[1])*height/1680, 75*width/1680*8/game_dim[0], 75*height/1680*8/game_dim[1]);
        }
            
        if (mousePressed) {
            
            if (this.movable && ((this.loc[0]-1)*200*8/game_dim[0]+40)*width/1680 < mouseX && mouseX < ((this.loc[0])*200*8/game_dim[0]+40)*width/1680 && (1640-this.loc[1]*200*8/game_dim[1])*height/1680 < mouseY && mouseY < (1640-(this.loc[1]-1)*200*8/game_dim[1])*height/1680) {
                this.stat = selected_piece.stat;
                this.crowned = selected_piece.crowned;
                selected_piece.stat = '-';
                selected_piece.crowned = false;
                this.movable = false;
                if ((this.loc[1] + selected_piece.loc[1]) % 2 == 0) {
                    board.grid[(this.loc[1] + selected_piece.loc[1])/2-1][(this.loc[0] + selected_piece.loc[0])/2-1].stat = '-';
                    board.grid[(this.loc[1] + selected_piece.loc[1])/2-1][(this.loc[0] + selected_piece.loc[0])/2-1].crowned = false;
                    this.selected = true;
                    selected_piece = this;
                    force = false;
                    for (Subsquare[] r: board.grid) {
                        for (Subsquare s: r) {
                            s.moveCheck();
                            if (s.movable) {
                                if (abs(s.loc[0] - selected_piece.loc[0]) > 1) {
                                    force = true;
                                }
                                else {
                                    s.movable = false;
                                }
                            }
                        }
                    }
                }
                if (!force) {
                    selectReset();
                    if (player == 'r') {
                        player = 'b';
                    }
                    else if (player == 'b') {     
                        player = 'r';
                    }
                }
                
            }
            
            else if (this.stat == player && ((this.loc[0]-1)*200*8/game_dim[0]+40)*width/1680 < mouseX && mouseX < ((this.loc[0])*200*8/game_dim[0]+40)*width/1680 && (1640-this.loc[1]*200*8/game_dim[1])*height/1680 < mouseY && mouseY < (1640-(this.loc[1]-1)*200*8/game_dim[1])*height/1680 && !force) {
                this.selected = true;
                selected_piece = this;
            }
                
            else if (!force) {
                this.selected = false;
            }
            
        }
                
        if (!force) {
            this.moveCheck();
        }
            
        if (this.movable) {
            fill(0, 256, 0, 150);
            rect(((this.loc[0]-1)*200*8/game_dim[0]+40)*width/1680, (1640-this.loc[1]*200*8/game_dim[1])*height/1680, 200*width/1680*8/game_dim[0], 200*height/1680*8/game_dim[1]);
        }
        
    }
    
}
                
class Gameboard {
      
    int[] dim;
    Subsquare[][] grid;
    
    Gameboard(int[] dim) {
        this.dim = dim;
        this.grid = new Subsquare[this.dim[1]][this.dim[0]];
        for (int j = 1; j < this.dim[1]+1; j++) {
            for (int i = 1; i < this.dim[0]+1; i++) {
                int[] pos = {i, j};
                grid[j-1][i-1] = new Subsquare(pos, '-');
            }
        }
    }
        
    void make() {
        
        for (Subsquare[] j: this.grid) {
            for (Subsquare i: j) {
                i.make();
            }
        }
        
    }
                
    void reset() {
        
        for (Subsquare[] r: this.grid) {
            for (Subsquare s: r) {
                if ((s.loc[0] + s.loc[1]) % 2 == 0) {
                    if (s.loc[0] + (s.loc[1]-1)*game_dim[0] <= 3*game_dim[0]) {
                        s.stat = 'r';
                    }
                    else if (s.loc[0] + (s.loc[1]-1)*game_dim[0] > (game_dim[1]-3)*game_dim[0]) {
                        s.stat = 'b';
                    }
                }
            }
        }
        
    }
    
}
        