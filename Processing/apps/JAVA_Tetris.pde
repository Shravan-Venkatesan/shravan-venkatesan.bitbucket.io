Tetromino piece;
Tetromino next_piece;
GameSpace space;
int time;
int level;
float speed;
int score;
int lines;
int total_lines;
int combo;
int soft;
int hard;
String tspin;
int tcount_mini;
int tcount_max;
int tetriscount;
boolean sing;
int[] gamesize = {10, 18};
int sizeX = 100*gamesize[0] + 720;
int sizeY = 100*gamesize[1] + 80;
float aspect = window.innerWidth / window.innerHeight;

void setup() {

    if (sizeX / sizeY > aspect) {
       size(sizeX*window.innerWidth/sizeX, sizeY*window.innerWidth/sizeX);
    }
    else {
       size(sizeX*window.innerHeight/sizeY, sizeY*window.innerHeight/sizeY);
    }
    textSize(75);
    float[] piece_loc = {floor(gamesize[0]/2), gamesize[1]-2};
    piece = randomPiece(piece_loc);
    piece.in_play = true;
    float[] next_loc = {gamesize[0]*1.4, gamesize[1]*15/18};
    next_piece = randomPiece(next_loc);
    space = new GameSpace(gamesize);
    time = millis();
    level = 1;
    speed = 1;
    score = 0;
    lines = 0;
    total_lines = 0;
    combo = 0;
    soft = 0;
    hard = 0;
    tspin = "none";
    tcount_mini = 0;
    tcount_max = 0;
    tetriscount = 0;
    sing = false;
    
}
    
void draw() {
    
    gameScreen();
    space.make();
    piece.make();
    if (next_piece.letter == 'I') {
        float[] piece_loc = {gamesize[0]*1.4-0.5, gamesize[1]*15/18+0.5};
        next_piece.loc = piece_loc;
    }
    else if (next_piece.letter == 'O') {
        float[] piece_loc = {gamesize[0]*1.4-0.5, gamesize[1]*15/18};
        next_piece.loc = piece_loc;
    }
    next_piece.make();
    level = 1 + floor(total_lines/10);
    speed = 1 + 0.2*level;
    if (millis() > time + 500/speed) {
        if (piece.boundCheck("down")) {
            piece.loc[1] = piece.loc[1] - 1;
            time = millis();
            tspin = "none";
        }
        else {
            piece.lock();
            float[] loc = {floor(gamesize[0]/2), gamesize[1]-2};
            piece = new Tetromino(next_piece.letter, loc, true, 0);
            float[] next_loc = {gamesize[0]*1.4, gamesize[1]*15/18};
            next_piece = randomPiece(next_loc);
        }
    }
    
    fill(0, 0, 0);
    text(str(score), 1200*width/1720, 800*height/1880);
    text(str(level), 1200*width/1720, 1000*height/1880);
    noFill();
    
}
    
void keyPressed() {
                
    if ((key == 'a') && piece.boundCheck("left") && hard == 0) {
        piece.loc[0] = piece.loc[0] - 1;
        tspin = "none";
    }
    else if ((key == 'd') && piece.boundCheck("right") && hard == 0) {
        piece.loc[0] = piece.loc[0] + 1;
        tspin = "none";
    }
    else if ((key == 's') && piece.boundCheck("down") && hard == 0) {
        piece.loc[1] = piece.loc[1] - 1;
        soft = soft + 1;
        tspin = "none";
    }
    else if ((key == 'w') && hard == 0) {
        float[] pos = {piece.loc[0]+1, piece.loc[1]-1};
        Tetromino tet = new Tetromino(piece.letter, piece.loc, false, piece.rotation + 1);
        Tetromino tet2 = new Tetromino(piece.letter, pos, false, piece.rotation + 1);
        if (tet.overlapCheck()) {
            piece.rot("C");
            if (piece.letter == 'T') {
                tspin = "max";
            }
        }
        else if (piece.letter == 'T' && tet2.overlapCheck()) {
            piece.rot("C");
            float[] loc = {piece.loc[0]+1, piece.loc[1]-1};
            piece.loc = loc;
            tspin = "mini";
        }
    }
    else if ((key == 'q') && hard == 0) {
        float[] pos = {piece.loc[0]-1, piece.loc[1]-1};
        Tetromino tet = new Tetromino(piece.letter, piece.loc, false, piece.rotation - 1);
        Tetromino tet2 = new Tetromino(piece.letter, pos, false, piece.rotation + 1);
        if (tet.overlapCheck()) {
            piece.rot("CC");
            if (piece.letter == 'T') {
                tspin = "max";
            }
        }
        else if (piece.letter == 'T' && tet2.overlapCheck()) {
            piece.rot("CC");
            float[] loc = {piece.loc[0]-1, piece.loc[1]-1};
            piece.loc = loc;
            tspin = "mini";
        }
    }
    else if (key == ' ') {
        while (piece.boundCheck("down")) {
            piece.loc[1] = piece.loc[1] - 1;
            hard = hard + 1;
            tspin = "none";
        }
    }
    
}
            
void gameScreen() {
    
    strokeWeight(40);
    stroke(256, 256, 206);
    fill(0, 0, 0);
    rect(20*width/1720, 20*height/1880, 1040*width/1720, 1840*height/1880);
    rect(1060*width/1720, 20*height/1880, 640*width/1720, 640*height/1880);
    fill(256, 256, 206);
    rect(1060*width/1720, 660*height/1880, 640*width/1720, 1200*height/1880); 
    
}
    
Tetromino randomPiece(float[] loc) {
    
    float r = random(7);
    
    if (r < 1) {
        return new Tetromino('O', loc);
    }
    else if (r < 2) {
        return new Tetromino('J', loc);
    }
    else if (r < 3) {
        return new Tetromino('L', loc);
    }
    else if (r < 4) {
        return new Tetromino('I', loc);
    }
    else if (r < 5) {
        return new Tetromino('S', loc);
    }
    else if (r < 6) {
        return new Tetromino('T', loc);
    }
    else if (r < 7) {
        return new Tetromino('Z', loc);
    }
    
    return null;
    
}
    
void chooseColor(char letter) {
    
    if (letter == 'I') {
        fill(0, 256, 256);
    }
    else if (letter == 'J') {
        fill(0, 106, 256);
    }
    else if (letter == 'L') {
        fill(256, 156, 0);
    }
    else if (letter == 'O') {
        fill(256, 256, 0);
    }
    else if (letter == 'S') {
        fill(0, 256, 0);
    }
    else if (letter == 'T') {
        fill(206, 0, 206);
    }
    else if (letter == 'Z') {
        fill(256, 0, 0);
    }
    else {
        fill(0, 0, 0);
    }
    
}
        
void updateScore() {
    
    combo = combo + 1;
    if (tspin == "mini") {
        tcount_mini = tcount_mini + 1;
        tcount_max = 0;
    }
    else if (tspin == "max") {
        tcount_max = tcount_max + 1;
        tcount_mini = 0;
    }
    else if (tspin == "none") {
        tcount_mini = 0;
        tcount_max = 0;
    }
    
    if (lines != 4) {
        tetriscount = 0;
    }
    
    if (lines == 1) {
        if (tspin == "none") {
            score = score + 100*level;
        }
        else if (tspin == "mini") {
            if (tcount_mini == 1) {
                score = score + 100*level;
            }
            else if (tcount_mini == 2) {
                score = score + 200*level;
            }
            else if (tcount_mini > 2) {
                score = score + 150*level;
            }
        }
        else if (tspin == "max") {
            if (tcount_max == 1) {
                score = score + 800*level;
            }
            else if (tcount_max == 2 && combo > 1) {
                score = score + 1600*level;
            }
            else if (tcount_max > 2 && combo > 1) {
                score = score + 1200*level;
            }
        }
    }
    else if (lines == 2) {
        if (tspin == "none" || tspin == "mini") {
            score = score + 300*level;
        }
        else if (tspin == "max") {
            if (tcount_max == 1) {
                score = score + 1200*level;
            }
            else if (tcount_max == 2) {
                score = score + 2400*level;
            }
            else if (tcount_max > 2) {
                score = score + 1800*level;
            }
        }
    }
    else if (lines == 3) {
        if (tspin == "none" || tspin == "mini") {
            score = score + 500*level;
        }
        else if (tspin == "max") {
            if (tcount_max == 1) {
                score = score + 1600*level;
            }
            else if (tcount_max == 2) {
                score = score + 3200*level;
            }
            else if (tcount_max > 2) {
                score = score + 2400*level;
            }
        }
    }
    else if (lines == 4) {
        tetriscount = tetriscount + 1;
        if (tetriscount == 1) {
            score = score + 800*level;
        }
        else if (tetriscount == 2) {
            score = score + 1600*level;
        }
        else if (tetriscount > 2) {
            score = score + 1200*level;
        }
    }
    else if (lines == 0) {
        if (tspin == "mini") {
            if (tcount_mini == 1) {
                score = score + 100*level;
            }
            else if (tcount_mini == 2) {
                score = score + 200*level;
            }
            else if (tcount_mini > 2) {
                score = score + 150*level;
            }
        }
        else if (tspin == "max") {
            score = score + 400*level;
        }
        combo = 0;
        
    }
        
    if (combo > 1) {
        if (combo == 2) {
            if (lines == 1 && sing) {
                score = score + 80*level;
            }
            else if (lines == 1 || sing) {
                score = score + 140*level;
            }
            else {
                score = score + 200*level;
            }
        }
        else if (combo > 2) {
            if (lines == 1) {
                score = score + 20*combo*level;
            }
            else {
                score = score + 50*combo*level;
            }
        }
    }
        
    score = score + soft + hard*2;
        
    if (lines == 1) {
        sing = true;
    }
    else {
        sing = false;
    }
        
    lines = 0;
    soft = 0;
    hard = 0;
    tspin = "none";
    
}

class Squarespace {
  
  float[] loc;
  char stat;
    
    Squarespace(float[] loc, char stat) {
        this.loc = loc;
        this.stat = stat;
    }
        
    void make() {
        strokeWeight(5);
        stroke(0, 0, 0);
        
        chooseColor(this.stat);
            
        rect(((this.loc[0]-1)*100*10/gamesize[0]+42.5)*width/1720, (1842.5-this.loc[1]*100*18/gamesize[1])*height/1880, 95*width/1720*10/gamesize[0], 95*height/1880*18/gamesize[1]);
    }
    
}
        
class GameSpace {
  
    int[] dim;
    Squarespace[][] plane;
    
    GameSpace(int[] dim) {
        this.dim = dim;
        this.plane = new Squarespace[dim[1]][dim[0]];
        
        for (int j = 1;  j < this.dim[1] + 1; j++) {
            for (int i = 1; i < this.dim[0] + 1; i++) {
                float[] ij = {i, j};
                this.plane[j-1][i-1] = new Squarespace(ij, '-');
            }
        }
    }
        
    boolean rowfill(Squarespace[] r) {
        
        for (Squarespace s: r) {
            if (s.stat == '-') {
                return false;
            }
        }
            
        return true;
        
    }
    
    void make() {
            
        for (Squarespace[] j: this.plane) {
            for (Squarespace i: j) {
                i.make();
            }
        }
            
        for (int j = 0; j < this.dim[1]; j++) {
            while (this.rowfill(this.plane[j])) {
                for (int j2 = j; j2 < this.dim[1] - 1; j2++) {
                    for (int i = 0; i < this.dim[0]; i++) {
                        this.plane[j2][i].stat = this.plane[j2+1][i].stat;
                    }
                }
                for (Squarespace s: this.plane[this.dim[1]-1]) {
                    s.stat = '-';
                }
                lines = lines + 1;
                total_lines = total_lines + 1;
            }
        }
        
    }
    
}
            
class Subpiece {
  
    char letter;
    float[] loc;
    int[] offset;
    
    Subpiece(char letter, float[] loc, int[] offset) {
        this.letter = letter;
        this.loc = loc;
        this.offset = offset;
    }
        
    void lock() {
        
        space.plane[(int)this.loc[1]+this.offset[1]-1][(int)this.loc[0]+this.offset[0]-1].stat = this.letter;
        
    }
    
    void make() {
        
        chooseColor(this.letter);
        strokeWeight(5);
        stroke(0, 0, 0);
        
        rect(((this.loc[0]+this.offset[0]-1)*100*10/gamesize[0]+42.5)*width/1720, (1842.5-(this.loc[1]+this.offset[1])*100*18/gamesize[1])*height/1880, 95*width/1720*10/gamesize[0], 95*height/1880*18/gamesize[1]);
        
    }
    
}
    
class Tetromino {
    
    char letter;
    float[] loc;
    Subpiece[] subs;
    boolean in_play;
    int rotation;
  
    Tetromino(char letter, float[] loc, boolean in_play, int rotation) {
        this.letter = letter;
        this.loc = loc;
        this.in_play = in_play;
        this.rotation = rotation;
        
        this.subgen();
    }
    
    Tetromino(char letter, float[] loc) {
        this(letter, loc, false, 0);
    }
        
    void rot(String dir) {
        
        if (dir == "C") {
            this.rotation = this.rotation + 1;
            if (this.rotation == 4) {
                this.rotation = 0;
            }
        }
        else if (dir == "CC") {
            this.rotation = this.rotation - 1;
            if (this.rotation == -1) {
                this.rotation = 3;
            }
        }
        
    }
        
    boolean boundCheck(String side) {
    
        if (side == "left") {
            for (Subpiece sp: this.subs) {
                if (sp.loc[0] + sp.offset[0] <= 1 || space.plane[(int)sp.loc[1]+sp.offset[1]-1][(int)sp.loc[0]+sp.offset[0]-2].stat != '-') {
                    return false;
                }
            }
        }
        else if (side == "right") {
            for (Subpiece sp: this.subs) {
                if (sp.loc[0] + sp.offset[0] >= space.dim[0] || space.plane[(int)sp.loc[1]+sp.offset[1]-1][(int)sp.loc[0]+sp.offset[0]].stat != '-') {
                    return false;
                }
            }
        }
        else if (side == "down") {
            for (Subpiece sp: this.subs) {
                if (sp.loc[1] + sp.offset[1] <= 1 || space.plane[(int)sp.loc[1]+sp.offset[1]-2][(int)sp.loc[0]+sp.offset[0]-1].stat != '-') {
                    return false;
                }
            }
        }
        
        return true;
        
    }
    
    boolean overlapCheck() {
        
        for (Subpiece sp: this.subs) {
            if (sp.loc[0] + sp.offset[0] < 1 || sp.loc[0] + sp.offset[0] > space.dim[0] || sp.loc[1] + sp.offset[1] < 2 || space.plane[(int)sp.loc[1]+sp.offset[1]-1][(int)sp.loc[0]+sp.offset[0]-1].stat != '-') {
                return false;
            }
        }
        
        return true;
        
    }
    
    void subgen() {
        
        this.subs = new Subpiece[4];
        int[][] offsets = new int[4][2];
        
        if (this.letter == 'I') {
            if (this.rotation == 0 || this.rotation == 2 ||  !this.in_play) {
                int[][] offlist = {{-1, 0}, {0, 0}, {1, 0}, {2, 0}};
                offsets = offlist;
            }
            else if (this.rotation == 1 || this.rotation == 3) {
                int[][] offlist = {{0, 2}, {0, 1}, {0, 0}, {0, -1}};
                offsets = offlist;
            }
        }
        else if (this.letter == 'J') {
            if (this.rotation == 0 || !this.in_play) {
                int[][] offlist = {{-1, 1}, {-1, 0}, {0, 0}, {1, 0}};
                offsets = offlist;
            }
            else if (this.rotation == 1) {
                int[][] offlist = {{1, 1}, {0, 1}, {0, 0}, {0, -1}};
                offsets = offlist;
            }
            else if (this.rotation == 2) {
                int[][] offlist = {{1, -1}, {1, 0}, {0, 0}, {-1, 0}};
                offsets = offlist;
            }
            else if (this.rotation == 3) {
                int[][] offlist = {{-1, -1}, {0, -1}, {0, 0}, {0, 1}};
                offsets = offlist;
            }
        }
        else if (this.letter == 'L') {
            if (this.rotation == 0 || !this.in_play) {
                int[][] offlist = {{-1, 0}, {0, 0}, {1, 0}, {1, 1}};
                offsets = offlist;
            }
            else if (this.rotation == 1) {
                int[][] offlist = {{0, 1}, {0, 0}, {0, -1}, {1, -1}};
                offsets = offlist;
            }
            else if (this.rotation == 2) {
                int[][] offlist = {{1, 0}, {0, 0}, {-1, 0}, {-1, -1}};
                offsets = offlist;
            }
            else if (this.rotation == 3) {
                int[][] offlist = {{0, -1}, {0, 0}, {0, 1}, {-1, 1}};
                offsets = offlist;
            }
        }
        else if (this.letter == 'O') {
            int[][] offlist = {{0, 0}, {1, 0}, {0, 1}, {1, 1}};
            offsets = offlist;
        }
        else if (this.letter == 'S') {
            if (this.rotation == 0 || this.rotation == 2 || !this.in_play) {
                int[][] offlist = {{-1, 0}, {0, 0}, {0, 1}, {1, 1}};
                offsets = offlist;
            }
            else if (this.rotation == 1 || this.rotation == 3) {
                int[][] offlist = {{0, -1}, {0, 0}, {-1, 0}, {-1, 1}};
                offsets = offlist;
            }
        }
        else if (this.letter == 'T') {
            if (this.rotation == 0 || !this.in_play) {
                int[][] offlist = {{-1, 0}, {0, 0}, {0, 1}, {1, 0}};
                offsets = offlist;
            }
            else if (this.rotation == 1) {
                int[][] offlist = {{0, 1}, {0, 0}, {1, 0}, {0, -1}};
                offsets = offlist;
            }
            else if (this.rotation == 2) {
                int[][] offlist = {{1, 0}, {0, 0}, {0, -1}, {-1, 0}};
                offsets = offlist;
            }
            else if (this.rotation == 3) {
                int[][] offlist = {{0, -1}, {0, 0}, {-1, 0}, {0, 1}};
                offsets = offlist;
            }
        }
        else if (this.letter == 'Z') {
            if (this.rotation == 0 || this.rotation == 2 || !this.in_play) {
                int[][] offlist = {{-1, 1}, {0, 1}, {0, 0}, {1, 0}};
                offsets = offlist;
            }
            else if (this.rotation == 1 || this.rotation == 3) {
                int[][] offlist = {{0, -1}, {0, 0}, {1, 0}, {1, 1}};
                offsets = offlist;
            }
        }
            
        for (int i = 0; i < 4; i++) {
            this.subs[i] = new Subpiece(this.letter, this.loc, offsets[i]);
        }
        
    }
            
    void lock() {
        
        for (Subpiece sp: this.subs) {
            sp.lock();
        }
            
        updateScore();
        
    }
    
    void make() { 
        
        this.subgen();
            
        for (int i = 0; i < 4; i++) {
            this.subs[i].make();
        }
        
    }
    
}
            